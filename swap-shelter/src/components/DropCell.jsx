import React, { useState } from 'react';

//Constants
import { ItemTypes } from "../logic/Constants";

//Custom components
import Card from "./Card";

/*External libraries*/
//React DnD docs - https://react-dnd.github.io/react-dnd/docs/overview
//Similar example - https://react-dnd.github.io/react-dnd/examples/dustbin/multiple-targets
//React DnD: useDrop docs - https://react-dnd.github.io/react-dnd/docs/api/use-drop
import { useDrop } from 'react-dnd';

function DropCell(props) {

    const [droppable, setDroppable] = useState(!props.lastDroppedItem);

    const [{ isActive }, drop] = useDrop({
        accept: ItemTypes.CARD,
        drop: props.onDrop,
        canDrop: () => droppable,
        collect: monitor => ({
            isActive: !!monitor.canDrop() && !!monitor.isOver()
        })
    });

    let card = null;

    function setCard() {
        if (props.lastDroppedItem) {
            card = <Card
                cardId={props.lastDroppedItem.cardId}
                info={props.lastDroppedItem.info}
                owned={props.lastDroppedItem.owned}
                draggable={props.cardDraggable}
                played={true}
                showValues={true}
                type={props.cardType}
                key={props.lastDroppedItem.cardId}
            />;
        } else {
            setDroppable(true);
        }
    }

    if (!droppable) {
        setCard();
    } else {
        if (droppable === !!props.lastDroppedItem) {
            setDroppable(false);
            setCard();
        }
    }

    const isHovered = isActive ? 'is-hovered' : '';

    let elem;
    switch(props.elem){
        case "X" : elem = null;
        break;
        case "cat" : elem = <img className="cell-element" src="images/cat-element.png" alt="c-elem"/>
        break;
        case "dog" : elem = <img className="cell-element" src="images/dog-element.png" alt="d-elem"/>
        break;
        default : elem = null;
    }

    return (
        <div ref={drop}  id={"cell-" + props.cellId} className={"gameCell col px-0 " + isHovered} >
            {elem ? elem : " "}
            {!!card && card}
        </div>
    );
}

export default DropCell;