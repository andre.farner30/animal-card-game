import React from 'react';

class GameChat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isWriting: false,
            message: undefined,
            history: []
        };
        this.chatBox = React.createRef();
    }

    componentDidMount() {
        this.props.socket.on('gamelobbymsg', (msg) => {
            this.setState(prevState => ({
                history: [...prevState.history, msg]
            }));
            if(this.chatBox.current) this.chatBox.current.scrollTop = this.chatBox.current.scrollHeight;
        });
    }

    sendHandler = (event) => {
        event.preventDefault();
        if (this.state.message !== "") {
            this.props.socket.emit('gamelobbymsg', this.state.message);
            this.setState({
                message: ""
            });
        }
    };

    chatHandler = (event) => {
        this.setState({
            message: event.target.value
        });
    };

    render() {
        let i = 0;
        return (
            <div className="col my-sm-3 px-0" id="chatSection">
                <ul id="chatList" ref={this.chatBox}>
                    {this.state.history.map(msg => (
                        <li className={msg.sent ? "left" : "right"} ref={this.chat} key={++i}>{msg.msg}</li>
                    ))}
                </ul>
                <form id="chatForm" onSubmit={this.sendHandler}>
                    <div className="input-group mb-3">
                        <input type="text" className="form-control nes-input" placeholder="Message"
                            aria-label="Recipient's username" aria-describedby="basic-addon2"
                            id="chatInput" autoComplete="off" value={this.state.message} name="chatInput" onChange={this.chatHandler} />
                        <div className="input-group-append">
                            <button type="submit" id="chatButton" className="nes-btn is-primary">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default GameChat;