import React from 'react';

function GameRules(props) {

    const style = {
        display: "inline-block",
        width: "50%",
        textAlign: "center",
        verticalAlign: "top"
    };

    const listItems = Object.keys(props.rules).map((rule, i) => (
        props.rules[rule] ? <li key={i} >{rule}</li> : null
    ));

    let listItems2;

    if (listItems.length > 3) {
        listItems2 = listItems.splice(3, listItems.length);
    }

    return (
        <div style={{ height: 115 }} className="col my-sm-3" id="rulesSection">
            <h4 style={{ textAlign: "center" }}>Rules:</h4>
            <ul style={style} id="rule-list1">
                {listItems}
            </ul>
            <ul style={style} id="rule-list2">
                {listItems2}
            </ul>
        </div>
    );
}

export default GameRules;