import React from 'react';

//Constants
import { ItemTypes } from "../../../logic/Constants";

//Custom components
import DropCell from '../../DropCell';

function GameBoard(props) {

    let cells = [];
    let row1;
    let row2;
    let row3;

    if (props.gameCells.length === 0) {

        props.board.forEach((row) => {
            for (let i = 0; i < row.length; i++) {
                cells.push(row[i]);
            }
        });

        const cellArr = cells.map((cell, i) => {
            return {
                elem: cell.elem,
                cellId: i,
                lastDroppedItem: null
            };
        });

        props.createGameCells(cellArr);
        row1 = cellArr.map((cell) => (
            <DropCell
                cellId={cell.cellId}
                elem={cell.elem}
                lastDroppedItem={cell.lastDroppedItem}
                cardDraggable={false}
                cardType={ItemTypes.CARD}
                onDrop={item => props.onDrop(cell.cellId, item)}
                key={cell.cellId}
            />
        ));

        row2 = row1.splice(3, 3);
        row3 = row1.splice(3, 3);
    } else {

        row1 = props.gameCells.map((cell) => (
            <DropCell
                cellId={cell.cellId}
                elem={cell.elem}
                lastDroppedItem={cell.lastDroppedItem}
                cardDraggable={false}
                cardType={ItemTypes.CARD}
                onDrop={item => props.onDrop(cell.cellId, item)}
                key={cell.cellId}
            />
        ));

        row2 = row1.splice(3, 3);
        row3 = row1.splice(3, 3);
    }

    return (
        <div className="col px-0 game-board">
            <div className="row mx-0 game-board-row justify-content-center">
                {row1}
            </div>
            <div className="row mx-0 game-board-row justify-content-center">
                {row2}
            </div>
            <div className="row mx-0 game-board-row justify-content-center">
                {row3}
            </div>
        </div>
    );
}

export default GameBoard;