import React from 'react';

function Score(props) {

    const cells = [];

    for (let i = 0; i < 10; i++) {
        cells.push(<div className={`col-1 border border-dark ${i < props.score ? "bg-primary" : "bg-danger"} rounded mx-sm-1`} key={i}></div>);
    }

    return (
        <div style={{ height: 115 }} className="col my-sm-3" id="scoreSection">
            <h4 style={{textAlign: "center" }}>Score:</h4>
            <div className="row justify-content-center my-sm-1" id="player-score-row">
                {cells.splice(0, cells.length / 2)}
            </div>
            <div className="row justify-content-center my-sm-1" id="opponent-score-row">
                {cells}
            </div>
        </div>
    );
}

export default Score;