import React from 'react';

//Constants
import { ItemTypes } from '../../../logic/Constants';

//Custom components
import Card from '../../Card';

function GameHand(props) {

    let cards = [];
    let cardRow1;
    let cardRow2;

    if (props.cards.length === 0) {

        cards = Object.keys(props.hand).map((card, i) => {
            return {
                cardId: i,
                info: props.hand[card],
                owned: props.isPlayer
            };
        });

        props.createCards(cards);

        cardRow1 = cards.map(card => (
            <Card
                cardId={card.cardId}
                info={card.info}
                owned={card.owned}
                draggable={props.draggable}
                played={false}
                showValues={props.showCardValues}
                type={ItemTypes.CARD}
                hideSourceOnDrag={true}
                key={card.cardId}
            />
        ));

        cardRow2 = cardRow1.splice(3, cardRow1.length);
    } else {

        cardRow1 = props.cards.map((card) => {
            if (typeof card !== "string") {
                return (
                    <Card
                        cardId={card.cardId}
                        info={card.info}
                        owned={card.owned}
                        draggable={props.draggable}
                        played={false}
                        showValues={props.showCardValues}
                        type={ItemTypes.CARD}
                        hideSourceOnDrag={true}
                        key={card.cardId}
                    />
                );
            } else {
                return (
                    <div className="dummyCard" key={card}></div>
                );
            }
        });

        cardRow2 = cardRow1.splice(3, cardRow1.length);
    }

    return (
        <div className="col my-sm-3">
            <div className="row mx-sm-2 my-sm-3 justify-content-center" id="player-bot-row">
                {cardRow1}
            </div>
            <div className="row mx-sm-2 my-sm-3 justify-content-center" id="player-top-row">
                {cardRow2}
            </div>
        </div>
    );
}

export default GameHand;