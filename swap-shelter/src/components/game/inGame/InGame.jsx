import React, { useState, useCallback, useEffect } from 'react';

//example - https://reacttraining.com/react-router/web/example/preventing-transitions
import { Prompt } from "react-router-dom";

//Custom components
import GamePlayer from '../GamePlayer';
import Score from './Score';
import CountdownBar from '../CountdownBar';
import GameRules from './GameRules';
import GameHand from './GameHand';
import GameBoard from './GameBoard';
import GameChat from '../GameChat';

import Sound from 'react-sound';

//Play logic
import {
    convertIdToCoords,
    convertCoordsToId,
    setCellItem,
    setCardInList,
    updateCardOwnership,
    getRandomCardInHand,
    getRandomEmptyCellId
} from "../../../logic/PlayLogic";

/*External libraries*/
//React Confirm Alert docs - https://www.npmjs.com/package/react-confirm-alert
//Demo - https://ga-mo.github.io/react-confirm-alert/demo/
import { confirmAlert } from "react-confirm-alert";

function InGame(props) {

    //-------------------- State --------------------

    const [gameCells, setGameCells] = useState([]);

    const [cards, setCards] = useState([]);

    const [opponentCards, setOpponentCards] = useState([]);

    const socket = useState(props.socket)[0];

    const [isPlayer1Turn, setPlayer1Turn] = useState(props.players.player.username === props.gameInfo.player1.username);

    const [turnTime, setTurnTime] = useState(props.turnTime);

    //-------------------- Game Logic --------------------

    function randomPlay(cards, gameCells) {
        const randomCard = getRandomCardInHand(cards);
        const randomCellId = getRandomEmptyCellId(gameCells);
        console.log(gameCells);
        console.log(cards);
        console.log(randomCard);
        console.log(randomCellId);

        setGameCells(setCellItem(gameCells, randomCellId, randomCard));
        setCards(setCardInList(cards, randomCard.cardId, "" + randomCard.cardId));
        socket.emit("playtrigger", { game: props.gameInfo, cardPos: convertIdToCoords(randomCellId), cardIndex: randomCard.cardId });
    }

    const handleDrop = useCallback(
        (index, item) => {
            setGameCells(setCellItem(gameCells, index, item));
            setCards(setCardInList(cards, item.cardId, "" + item.cardId));
            socket.emit("playtrigger", { game: props.gameInfo, cardPos: convertIdToCoords(index), cardIndex: item.cardId });
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [gameCells, cards]
    );

    // Test play
    // function updatePlayedCards(state, gameCells) {
    //     console.log(state);
    //     if (state.gameState.lastPlay.captured.length !== 0) {
    //         console.log("Captured before: ");
    //         console.log(gameCells);
    //         state.gameState.lastPlay.captured.forEach(cellCoords => {
    //             console.log("Captured cycle");
    //             gameCells = updateCardOwnership(gameCells, convertCoordsToId([cellCoords.y, cellCoords.x]));
    //         });
    //         console.log("Captured after: ");
    //         console.log(gameCells);
    //     } else if (state.gameState.lastPlay.same.length !== 0) {
    //         console.log("Same before: ");
    //         console.log(gameCells);
    //         state.gameState.lastPlay.same.forEach(cellCoords => {
    //             console.log("Same cycle");
    //             gameCells = updateCardOwnership(gameCells, convertCoordsToId([cellCoords.y, cellCoords.x]));
    //         });
    //         console.log("Same after: ");
    //         console.log(gameCells);
    //     } else if (state.gameState.lastPlay.plus.length !== 0) {
    //         console.log("Plus before: ");
    //         console.log(gameCells);
    //         state.gameState.lastPlay.plus.forEach(cellCoords => {
    //             console.log("Plus cycle");
    //             gameCells = updateCardOwnership(gameCells, convertCoordsToId([cellCoords.y, cellCoords.x]));
    //         });
    //         console.log("Plus after: ");
    //         console.log(gameCells);
    //     }
    //     if (state.gameState.lastPlay.combo.length !== 0) {
    //         console.log("Combo before: ");
    //         console.log(gameCells);
    //         state.gameState.lastPlay.combo.forEach(comboCard => {
    //             console.log("Combo cycle");
    //             gameCells = updateCardOwnership(gameCells, convertCoordsToId([comboCard.pos.y, comboCard.pos.x]));
    //         });
    //         console.log("Combo after: ");
    //         console.log(gameCells);
    //     }
    //     setGameCells(gameCells);
    // }

    function updatePlayedCards(state, gameCells) {
        if (state.gameState.lastPlay.captured.length !== 0) {
            state.gameState.lastPlay.captured.forEach(cellCoords => {
                gameCells = updateCardOwnership(gameCells, convertCoordsToId([cellCoords.y, cellCoords.x]));
            });
        } else if (state.gameState.lastPlay.same.length !== 0) {
            state.gameState.lastPlay.same.forEach(cellCoords => {
                gameCells = updateCardOwnership(gameCells, convertCoordsToId([cellCoords.y, cellCoords.x]));
            });
        } else if (state.gameState.lastPlay.plus.length !== 0) {
            state.gameState.lastPlay.plus.forEach(cellCoords => {
                gameCells = updateCardOwnership(gameCells, convertCoordsToId([cellCoords.y, cellCoords.x]));
            });
        }
        if (state.gameState.lastPlay.combo.length !== 0) {
            state.gameState.lastPlay.combo.forEach(comboCard => {
                gameCells = updateCardOwnership(gameCells, convertCoordsToId([comboCard.pos.y, comboCard.pos.x]));
            });
        }
        setGameCells(gameCells);
    }

    const playHandler = (state, gameCells, opponentCards, turnTime) => {
        if (!state.madePlay) {
            const playedCardIndex = state.playedCardIndex;
            const cellIndex = convertCoordsToId([state.gameState.lastPlay.firstCard.y, state.gameState.lastPlay.firstCard.x]);
            let newGameCells = setCellItem(gameCells, cellIndex, opponentCards[playedCardIndex]);
            updatePlayedCards(state, newGameCells);
            setOpponentCards(setCardInList(opponentCards, playedCardIndex, "" + playedCardIndex));
            setPlayer1Turn(true);
        } else {
            updatePlayedCards(state, gameCells);
            setPlayer1Turn(false);
        }
        props.updateGameInfo(state.gameState);
        setTurnTime(turnTime);
    };

    //-------------------- Socket listeners --------------------

    // listener gets declared multiple times/only way to pass in gameCells and opponentCards is to forsake ComponentDidMount functionality
    useEffect(() => {
        socket.removeAllListeners("playtrigger");
        socket.on("playtrigger", (msg) => {
            playHandler(msg, gameCells, opponentCards, turnTime);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [gameCells, opponentCards, turnTime]);

    useEffect(() => {
        socket.removeAllListeners("turntimer");
        socket.on("turntimer", (msg) => {
            console.log("Random play");
            randomPlay(cards, gameCells);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [cards, gameCells]);

    useEffect(() => {
        socket.on('gameEnd', function (msg) {
            endGame(msg);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        socket.on('postGame', function (msg) {
            if (!msg.rematch && msg.cond === 0) {
                window.onbeforeunload = null;
                window.location.replace(window.location.href.split("/").splice(0, 3).join('/'));
            }
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    //-------------------- Handle meta events --------------------

    function endGame(gameInfo) {
        let player = "" + gameInfo.player1.username === "" + props.players.player.username ? gameInfo.player1 : gameInfo.player2;
        let opponent = "" + gameInfo.player1.username === "" + props.players.opponent.username ? gameInfo.player1 : gameInfo.player2;
        setTurnTime(0);
        confirmAlert({
            customUI: ({ onClose }) => {
                return (
                    <div className="nes-container is-rounded with-title end-screen">
                        <p className="title">Game Over!</p>
                        <div className="row justify-content-center end-screen-top">
                            {player.score > 5 ? <div className="col end-screen-leftp">
                                <p className="nes-text is-primary"><b>{player.username}</b></p>
                                <img src="images/profilepic.gif" className="is-winner" alt="p1" />
                                <p className="nes-text is-success"><b>Winner!</b></p>
                            </div> :
                                <div className="col end-screen-leftp">
                                    <p className="nes-text is-primary"><b>{player.username}</b></p>
                                    <img src="images/profilepic.gif" alt="p1" />
                                </div>}
                            <div className="col end-screen-score">
                                <p>Score</p>
                                <h1><b className="nes-text is-primary" >{player.score}</b>
                                    <b>/</b>
                                    <b className="nes-text is-error">{opponent.score}</b></h1>
                            </div>
                            {opponent.score > 5 ? <div className="col end-screen-rightp">
                                <p className="nes-text is-primary"><b>{opponent.username}</b></p>
                                <img src="images/profilepic.gif" className="is-winner" alt="p2" />
                                <p className="nes-text is-success"><b>Winner!</b></p>
                            </div> :
                                <div className="col end-screen-rightp">
                                    <p className="nes-text is-primary"><b>{opponent.username}</b></p>
                                    <img src="images/profilepic.gif" alt="p2" />
                                </div>}
                        </div>
                        <div className="row justify-content-center end-screen-bottom">
                            <button className="nes-btn is-primary" onClick={() => {
                                socket.emit("postGame", { cond: 1, rematch: true })
                            }}>
                                Rematch
                            </button>
                            <button className="nes-btn is-primary" onClick={() => {
                                socket.emit("postGame", { cond: 1, rematch: false })
                            }}>
                                Back to Lobby
                            </button>
                            <button className="nes-btn is-error" onClick={() => {
                                socket.emit("postGame", { cond: 0, rematch: false })
                            }}>
                                Quit
                            </button>
                        </div>
                    </div>
                )
            },
            closeOnEscape: false,
            closeOnClickOutside: false
        });
    }

    const handleConcede = (event) => {
        event.preventDefault();
        confirmAlert({
            customUI: ({ onClose }) => {
                return (
                    <div className="nes-container is-rounded with-title concede-screen">
                        <p className="title">Concede</p>
                        <p>You'll lose the game, are you sure?</p>
                        <img src="images/end-screen-cat.png" alt="sad"/>
                        <div className="row justify-content-center">
                            <button className="nes-btn is-primary" onClick={() => {
                                props.socket.emit("gameConcede", { gameInfo: props.gameInfo, player: props.players.player.username });
                            }}>
                                Yes
                            </button>
                            <button className="nes-btn is-error" onClick={onClose}>No</button>
                        </div>
                    </div>
                )
            }
        });
    };

    //-------------------- Window events --------------------

    useEffect(() => {
        window.onbeforeunload = function () {
            return "If you leave this match midway it will count as your loss.";
        };

        return () => {
            window.onbeforeunload = null;
        }
    }, []);

    //-------------------- Render --------------------

    return (
        <div id="lobby-section" className="row h-100 justify-content-center">
            {/* <Sound
                url="sound/game-music.mp3"
                playStatus={Sound.status.PLAYING}
                playFromPosition={300/}
                volume={100}
                loop={true}
            /> */}
            <div id="leftSection"
                className={isPlayer1Turn ? "col-3 nes-container is-rounded mx-sm-1 my-sm-3 player-turn" :
                    "col-3 nes-container is-rounded mx-sm-1 my-sm-3"} >
                <Score score={props.players.player.username === props.gameInfo.player1.username ? props.gameInfo.player1.score : props.gameInfo.player2.score} />
                <GamePlayer side="left" name={props.players.player.username} />
                <CountdownBar time={turnTime} start={isPlayer1Turn} side="left" />
                <GameHand
                    hand={props.players.player.hand}
                    isPlayer={true}
                    draggable={isPlayer1Turn}
                    createCards={setCards}
                    cards={cards}
                    showCardValues={true}
                />
                <GameChat socket={socket} />
            </div>
            <div id="middleSection" className="mx-sm-2 my-sm-3 col-4 px-0">
                <div className="ingame-logo">
                    <img src="images/ESWLogo.png" alt="logo" />
                </div>
                <GameBoard
                    board={props.gameInfo.board}
                    createGameCells={setGameCells}
                    gameCells={gameCells}
                    onDrop={handleDrop}
                />
            </div>
            <div id="rightSection" className={!isPlayer1Turn ? "col-3 nes-container is-rounded mx-sm-1 my-sm-3 player-turn" :
                "col-3 nes-container is-rounded mx-sm-1 my-sm-3"} >
                <GameRules rules={props.gameInfo.options} />
                <GamePlayer side="right" name={props.players.opponent.username} />
                <CountdownBar time={turnTime} start={!isPlayer1Turn} side="right" />
                <GameHand
                    hand={props.players.opponent.hand}
                    isPlayer={false}
                    draggable={false}
                    createCards={setOpponentCards}
                    cards={opponentCards}
                    showCardValues={props.gameInfo.options.open}
                />
                <button id="concede-button" className="nes-btn is-error" onClick={handleConcede}>Concede</button>
            </div>
            <Prompt
                when={true}
                message={location =>
                    "If you leave this match midway it will count as your loss"
                }
            />
        </div>
    );
}

export default InGame;