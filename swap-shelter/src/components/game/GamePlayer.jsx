import React from 'react';

function GamePlayer(props) {
    return (
        <div id={props.side === "left" ? "left-player" : "right-player"} className="row mx-0 player-info-section">
            <img className="player-pic-small" src="images/profilepic.gif" alt="user" />
            <div className="col px-0 player-info-top">
                <p className={props.side === "left" ? "nes-text is-primary" : "nes-text is-error"}><b>{props.name}</b></p>
                <p>Games: 5</p>
                <p>Wins: 3</p>
                <p>Losses: 2</p>
            </div>
        </div>
    );
}

export default GamePlayer;