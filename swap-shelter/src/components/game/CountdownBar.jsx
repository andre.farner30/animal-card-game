import React from 'react';

class CountdownBar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            time: 0
        };
        this.timer = 0;
    }

    componentDidMount() {
        this.setState({
            time: this.props.time
        });
    }

    componentDidUpdate(prevProps) {
        if (this.props.start !== prevProps.start) {
            if (this.props.start) {
                clearInterval(this.timer);
                this.setState({
                    time: this.props.time
                },
                    () => {
                        if (this.props.start) {
                            this.startTimer();
                        }
                    });
            } else {
                clearInterval(this.timer);
            }
        }
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    startTimer = () => {
        if (this.state.time > 0) {
            this.timer = setInterval(this.countDown, 1000);
        }
    };

    countDown = () => {
        // Remove one second, set state so a re-render happens.
        let time = this.state.time - 1;
        this.setState({
            time: time
        });

        // Check if we're at zero.
        if (time === 0) {
            clearInterval(this.timer);
        }
    };

    render() {
        return (
            <div className="col mt-sm-3 mx-0 px-0" id="leftTimerSection">
                <progress className={this.props.side === "left" ? "nes-progress is-primary" : "nes-progress is-error"}
                    value={this.props.start ? `${(this.state.time - 1) / (this.props.time - 1) * 100}` : '0'} max="100">
                </progress>
            </div>
        );
    }
}

export default CountdownBar;