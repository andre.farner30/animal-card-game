import React from "react";
import { Link } from "react-router-dom";

class JoinHost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            roomName: "",
            error: ""
        };
    }

    componentDidMount() {
        this.props.socket.on('fullLobby', (msg) => {
            if (msg) {
                this.setState({
                    error: "Room already exists or is full"
                });
            }
        });
    }

    handleLobby = (e, joinOrHost) => {
        e.preventDefault();
        if (this.state.roomName === "") {
            this.setState({
                error: "Please enter a valid name"
            });
        } else {
            joinOrHost ? this.props.socket.emit("joinroom", { name: this.state.roomName, joinorhost: "host", player: this.props.user }) :
                this.props.socket.emit("joinroom", { name: this.state.roomName, joinorhost: "join", player: this.props.user });
        }
    };

    handleChange= (e) => {
        e.preventDefault();
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    render() {
        return (
            <div id="host-join-row" className="row border justify-content-center h-100">
                <div className="col bg-light my-auto py-sm-3 h-j-col">
                    <div id="host-join-col">
                        <div className="logo-img-alt">
                            <Link to="/"><img src="images/ESWLogo.png" alt="logo" /></Link>
                        </div>
                        <div className="input-group">
                            <input onChange={this.handleChange} className="form-control my-lg-3 nes-input" name="roomName" value={this.state.roomName}
                                autoComplete="off" id="host-name-input" type="text" placeholder="Room Name" />
                        </div>
                        <div className="row bg-light justify-content-center">
                            <button id="host-button" className="nes-btn is-error" onClick={(e) => this.handleLobby(e, true)}>Host</button>
                            <button id="join-button" className="nes-btn is-primary" onClick={(e) => this.handleLobby(e, false)}>Join</button>
                        </div>
                        <span id="error-display" className="text-danger" style={{ visibility: this.state.error === "" ? "hidden" : "visible" }}>*{this.state.error}*</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default JoinHost;