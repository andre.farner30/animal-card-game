import React, { useState } from "react";
import { Link } from "react-router-dom";

//Custom components
import SocketContext from '../SocketContext';
import GameChat from '../GameChat';
import GameDecks from './GameDecks';
import GamePlayer from '../GamePlayer';
import LobbyRules from './LobbyRules';
import ReadySection from './ReadySection';

function Lobby(props) {

    const [ready, setReady] = useState(false);

    const [opponentReady, setOpponentReady] = useState(false);

    const [deck, setDeck] = useState("deck1");

    const selectDeck = (selectedDeck) => {
        setDeck(selectedDeck);
    };

    const readyHandler = (isReady) => {
        isReady ? setReady(true) : setReady(false);
    };

    const opponentReadyHandler = (isReady) => {
        isReady ? setOpponentReady(true) : setOpponentReady(false);
    };

    return (
        <div id="lobby-section" className="row h-100 justify-content-center">
            <div id="leftSection" className={ready ? "ready-player col-3 nes-container is-rounded mx-sm-1 my-sm-3" :
                "col-3 nes-container is-rounded mx-sm-1 my-sm-3"}>
                <GamePlayer side={"left"} name={props.lobbyInfo.player.username} />
                <GameDecks decks={props.lobbyInfo.player.decks} selectedDeck={selectDeck} />
                <div id="ready-state">
                    <h2>{ready ? <span className="nes-text is-success">Ready</span> : <span className="nes-text">Not Ready</span>}</h2>
                </div>
            </div>
            <div id="middleSection" className="col-3 nes-container is-rounded mx-sm-1 my-sm-3">
                <div id="middleLogo" className="logo-img-alt">
                    <Link to="/"><img src="images/ESWLogo.png" alt="Logo" /></Link>
                </div>
                <h3 id="roomName">Room: <u>{props.lobbyInfo.room}</u></h3>
                <SocketContext.Consumer>
                    {value => <LobbyRules int={ready} rules={props.lobbyInfo.rules} turnTime={props.lobbyInfo.turnTime} socket={value} />}
                </SocketContext.Consumer>
                <SocketContext.Consumer>
                    {value => props.lobbyInfo.opponent ?
                        <ReadySection rdyHandler={readyHandler} opponentRdyHandler={opponentReadyHandler} player={props.lobbyInfo.player.username}
                            deck={deck} socket={value} /> : null
                    }
                </SocketContext.Consumer>
                <SocketContext.Consumer>
                    {value => <GameChat socket={value}/>}
                </SocketContext.Consumer>
            </div>
            {props.lobbyInfo.opponent ?
                <div id="rightSection" className={opponentReady ? "ready-player col-3 nes-container is-rounded mx-sm-1 my-sm-3" :
                    "col-3 nes-container is-rounded mx-sm-1 my-sm-3"}>
                    <GamePlayer side={"right"} name={props.lobbyInfo.opponent.username} />
                    <div className="col mx-sm-1 my-sm-3 opponent-stats">
                        <h3 className="nes-text is-error">STATS</h3>
                    </div>
                    <div id="ready-state">
                        <h2>{opponentReady ? <span className="nes-text is-success">Ready</span> : <span className="nes-text">Not Ready</span>}</h2>
                    </div>
                </div> :
                <div id="rightSection" className="col-3 nes-container is-rounded mx-sm-1 my-sm-3">
                </div>
            }
        </div>
    );
}

export default Lobby;