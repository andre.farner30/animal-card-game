import React from 'react';

function LobbyRules(props) {

    const ruleHandle = (e) => {
        props.socket.emit("rulechange", { checked: e.target.checked, rule: e.target.name })
    };

    const turnTimeChange = (e) => {
        props.socket.emit("turntimechange", e.target.value);
    };

    const rules = Object.keys(props.rules).map(rule =>
        <div key={rule} className="lobbyRule">
            <label htmlFor={rule + "-check"}>
                <input disabled={props.int} onChange={ruleHandle} checked={props.rules[rule]}
                    name={rule} type="checkbox" className="ruleCheckbox nes-checkbox" id={rule + "-check"} />
                <span>{rule}</span>
            </label>
        </div>
    );

    const turnTime =
        <div className="nes-select">
            <select
                required
                id="time-select"
                value={props.turnTime}
                name="turntime"
                onChange={turnTimeChange}
            >
                <option value={15}>15s</option>
                <option value={30}>30s</option>
                <option value={60}>60s</option>
                <option value={120}>120s</option>
            </select>
        </div>;

    return (
        <div id="middleRules" className="mx-0 my-sm-3">
            <b>Rules: </b><br />
            {rules}
            <label htmlFor="time-select">Turn time: </label>
            {turnTime}
        </div>
    );
}

export default LobbyRules;