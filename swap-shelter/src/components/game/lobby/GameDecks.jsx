import React, { useState, useEffect } from 'react';

//Custom components
import Deck from './Deck';

function GameDecks(props) {

    const [selectedDeck, setSelectedDeck] = useState("deck1");

    const deckHandler = (deck) => {
        setSelectedDeck(deck);
    };

    useEffect(
        () => {
            props.selectedDeck(selectedDeck);
        }
    );

    const decks = Object.keys(props.decks).map(deck =>
        <Deck key={deck} dVal={deck} dHandler={deckHandler} picked={selectedDeck} deck={props.decks[deck]} />
    );

    return (
        <div id="left-player-decks" className="col mx-0 my-sm-3">
            <h3 className="nes-text is-primary" >DECKS</h3>
            {decks}
        </div>
    );
}

export default GameDecks;