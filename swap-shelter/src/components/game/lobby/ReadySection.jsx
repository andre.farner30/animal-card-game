import React from 'react';

//Custom components
import CountdownBar from '../CountdownBar';

class ReadySection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ready: false,
            timerStart: false
        };
    }

    componentDidMount() {
        this.props.socket.on("playerisready", (msg) => {
            if (msg === 1) this.props.opponentRdyHandler(true);
            if (msg === 1 && this.state.ready) {
                this.props.socket.emit("playerisready", { state: 2, deck: undefined });
            }
            if (msg === 2) {
                this.setState({
                    timerStart: true
                });
            } else if (msg === 0) {
                this.setState({
                    timerStart: false,
                    ready: false
                }, () => {
                    (this.props.rdyHandler(this.state.ready));
                    this.props.opponentRdyHandler(false);
                });
            }
        });
    }

    readyHandler = (e) => {
        e.preventDefault();
        if (this.state.ready) {
            this.setState({
                ready: false
            },
                () => {
                    this.props.socket.emit("playerisready", { state: 0, deck: undefined, playerName: this.props.player });
                    this.props.rdyHandler(this.state.ready);
                });
        } else {
            this.setState({
                ready: true
            },
                () => {
                    this.props.socket.emit("playerisready", { state: 1, deck: this.props.deck, playerName: this.props.player });
                    this.props.rdyHandler(this.state.ready);
                });
        }
    };

    render() {
        return (
            <div className="mx-sm-2 my-sm-3">
                <CountdownBar time={5} start={this.state.timerStart} side="left" />
                <button onClick={this.readyHandler} className="nes-btn is-primary btn-block" id="lobby-ready-button" data-toggle="button" aria-pressed="false">Ready</button>
            </div>
        );
    }
}

export default ReadySection;