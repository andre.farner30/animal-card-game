import React from 'react';

function Deck(props) {

    const pickDeck = () => {
        props.dHandler(props.dVal);
    }

    const element = (card) => {
        switch(card.elemental){
            case "cat" : return "C";
            case "dog" : return "D";
            case "X" : return "-";
            default : return "-";
        }
    }

    const deck = props.deck.cards.map((card, i) =>
        <div className="col border bg-primary lobbyCard" key={i}>
            <div>
                {card.up}<br />{card.left} {element(card)} {card.right}<br />{card.down}
            </div>
        </div>
    );

    return (
        <div className={"row justify-content-center my-2 " +
            (props.deck.cards.length ? "playerDeck " : "inactiveDeck ") +
            (props.picked === props.dVal ? "selectedDeck" : "")}
            onClick={(props.deck.cards.length ? pickDeck : undefined)}>
            <div className="col">
                <div className="row justify-content-center lobbyDeck">
                    {deck}
                </div>
                <div className="lobbyDeckName">
                    <h5>{props.deck.name}</h5>
                </div>
            </div>
        </div>
    );

}

export default Deck;