import React from "react";
import { Link } from "react-router-dom";
import LoginRegPartial from "./login-register/LoginRegPartial";

import '../styles/header.css';
class Header extends React.Component {
    constructor(props) {
        super(props);
        this.style = {
            height: "100px"
        };
    }

    render() {
        return (
            <header>
                <div className="row justify-content-center">
                    <div className="col-2">
                        <Link to="/backoffice" className="header-link float-right">Administration</Link>
                    </div>
                    <div className="col-2">
                        <Link to="/shop" className="header-link float-right"><i className="far fa-gem"></i>Shop</Link>
                    </div>
                    <div className="col-3">
                        <div className="logo-img">
                            <Link to="/"><img src="images/ESWLogo.png" alt="logo" /></Link>
                        </div>
                    </div>
                    <div className="col-2">
                        <Link to="/leaderboard" className="header-link float-left"><i className="fas fa-users"></i>Leaderboard</Link>
                    </div>
                    <div className="col-2">
                        <LoginRegPartial />
                    </div>
                </div>
            </header>
        );
    }
}

export default Header;