import React from "react";

//Custom styles
import "../../styles/shop/shopTabs.css";

function ShopTabs(props) {

    function handleClick(event) {
        event.preventDefault();
        props.changeTab(event.currentTarget.name);
    }

    return (
        <div className="row sTabs mt-lg-5" >
            <div className="col nes-container shop-tab">
                <a href="/" onClick={handleClick} className="col" id="shopCosmeticsTab" name="cosmetics">COSMETICS</a>
            </div>
            <div className="col nes-container shop-tab">
                <a href="/" onClick={handleClick} className="col" id="shopCardsTab" name="cards">CARDS</a>
            </div>
        </div>
    );
}

export default ShopTabs;