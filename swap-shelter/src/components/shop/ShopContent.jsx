import React, { Component } from "react";

//Axios docs - https://www.npmjs.com/package/axios
import axios from "axios";

//Custom styles
import "../../styles/shop/shopContent.css";

//Custom components
import ShopTabs from "./ShopTabs";
import ShopCosmeticsSearch from "./cosmetics/ShopCosmeticsSearch";
import ShopCosmeticsList from "./cosmetics/ShopCosmeticsList";
import ShopCardTypes from "./cards/ShopCardTypes";
import ShopCards from "./cards/ShopCards";

class ShopContent extends Component {
    constructor() {
        super();
        this.state = {
            tabKey: "cosmetics",
            cosmeticsType: "all",
            cardsType: "dogs",
            packAmount: "1"
        };
    }

    changeTab = (newKey) => {
        this.setState({
            tabKey: newKey,
            cosmeticsType: "all",
            cardsType: "dogs",
            packAmount: "1"
        });
    }

    selectCosmeticsType = (newKey) => {
        this.setState({ cosmeticsType: newKey });
    }

    selectCardType = (newKey) => {
        this.setState({ cardsType: newKey });
    }

    selectPackAmount = (newKey) => {
        this.setState({ packAmount: newKey });
    }

    confirmPurchase = async (packAmount) => {
        let cards = packAmount * 3;
        let arr = [];
        for (let i = 0; i < cards; i++) {
            arr.push(Math.floor(Math.random() * 20) + 1)
        }
        let that = this.props.updateUser;
        await axios.post("/requests/newcards/" + this.props.email, {
            cards: arr
        }).then(function (response) {
            that(response.data);
        });
    }

    render() {
        return (
            <div>
                <ShopTabs changeTab={this.changeTab} />
                <div className="col shop-content">
                    {
                        this.state.tabKey === "cosmetics" ?
                            <div className="row">
                                <ShopCosmeticsSearch selectCosmeticsType={this.selectCosmeticsType} />
                                <ShopCosmeticsList cosmeticsType={this.state.cosmeticsType} />
                            </div> :
                            <div className="row">
                                <ShopCardTypes selectCardType={this.selectCardType} />
                                <ShopCards
                                    selectPackAmount={this.selectPackAmount}
                                    cardsType={this.state.cardsType}
                                    packAmount={this.state.packAmount}
                                    confirmPurchase={this.confirmPurchase}
                                />
                            </div>
                    }
                </div>
            </div>
        );
    }
}

export default ShopContent;