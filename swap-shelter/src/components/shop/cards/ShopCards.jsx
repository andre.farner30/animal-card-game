import React from "react";

//Custom styles
import "../../../styles/shop/cards/shopCards.css";

//External styles
import "react-confirm-alert/src/react-confirm-alert.css";

/*External libraries*/
//React Confirm Alert docs - https://www.npmjs.com/package/react-confirm-alert
//Demo - https://ga-mo.github.io/react-confirm-alert/demo/
import { confirmAlert } from "react-confirm-alert";

import CardPack from './CardPack'

class ShopCards extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeIndex: 0,
            packSize: "1"
        }
        this.handleChange = this.handleChange.bind(this);
    }



    handleChange(event) {
        this.setState({
            packSize: event.target.name
        });
        this.props.selectPackAmount(event.currentTarget.name);
        console.log(this.state.packSize);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        confirmAlert({
            customUI: ({ onClose }) => {
                return (
                    <div className="nes-container is-rounded with-title">
                        <p className="title">Confirm purchase</p>
                        <p>Are you sure you want to buy {this.props.packAmount} pack{this.props.packAmount > 1 ? "s" : ""}?</p>
                        <div className="row justify-content-center">
                            <button className="nes-btn is-primary" onClick={() => {
                                this.props.confirmPurchase(this.props.packAmount);
                                onClose();
                            }}>
                                Confirm
                            </button>
                            <button className="nes-btn is-error" onClick={onClose}>Cancel</button>
                        </div>
                    </div>
                )
            }
        });
    };

    createCardPacks = () => {
        let packs = [];
        for (let i = 0; i < this.state.packSize; i++) {
            packs.push(<CardPack key={i} />);
        }
        return packs;
    }

    render() {

        return (
            <div className="col shop-cards-content">
                <div className={this.state.packSize === "50" ? "row justify-content-center too-many-cards" : "row justify-content-center"}
                    id="shopCardsListField">
                    {this.createCardPacks()}
                    {this.props.cardsType === "dogs" ?
                        <img className="shop-cards-pic-1" src="images/shop/dog1.png" alt="dog1" /> :
                        <img className="shop-cards-pic-1" src="images/shop/cat2.png" alt="cat1" />}
                    {this.props.cardsType === "dogs" ?
                        <img className="shop-cards-pic-2" src="images/shop/dog2.gif" alt="dog2" /> :
                        <img className="shop-cards-pic-2" src="images/shop/cat1.gif" alt="cat2" />}
                </div>
                <div className="nes-container is-rounded is-dark with-title pack-amount">
                    <p className="title">Amount of Packs:</p>
                    <label>
                        <input onChange={this.handleChange} name={"1"}
                            type="radio" className="nes-radio is-dark" checked={this.state.packSize === "1"} />
                        <span>1</span>
                    </label>
                    <label>
                        <input onChange={this.handleChange} name={"2"}
                            type="radio" className="nes-radio is-dark" checked={this.state.packSize === "2"} />
                        <span>2</span>
                    </label>
                    <label>
                        <input onChange={this.handleChange} name={"5"}
                            type="radio" className="nes-radio is-dark" checked={this.state.packSize === "5"} />
                        <span>5</span>
                    </label>
                    <label>
                        <input onChange={this.handleChange} name={"10"}
                            type="radio" className="nes-radio is-dark" checked={this.state.packSize === "10"} />
                        <span>10</span>
                    </label>
                    <label>
                        <input onChange={this.handleChange} name={"15"}
                            type="radio" className="nes-radio is-dark" checked={this.state.packSize === "15"} />
                        <span>15</span>
                    </label>
                    <label>
                        <input onChange={this.handleChange} name={"30"}
                            type="radio" className="nes-radio is-dark" checked={this.state.packSize === "30"} />
                        <span>30</span>
                    </label>
                    <label>
                        <input onChange={this.handleChange} name={"50"}
                            type="radio" className="nes-radio is-dark" checked={this.state.packSize === "50"} />
                        <span>50</span>
                    </label>
                </div>
                <div className="row" id="shopCardsInfoSection">
                    <div className="col" id="shopCardsListDescription">Each pack contains at least one 3 star card!</div>
                    <div className="col-4">
                        <h4 id="shopCardsPrice"><b className="nes-text is-error">{this.props.packAmount}</b> Treat(s)</h4>
                        <button href="/" onClick={this.handleSubmit} className="nes-btn is-error" id="shopCardListBuyButton">
                            BUY
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default ShopCards;