import React from "react";

//Custom styles
import "../../../styles/shop/cards/shopCardTypes.css";

class ShopCardTypes extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            activeIndex: 0
        }
        this.handleClick = this.handleClick.bind(this);
    }
    
    handleClick(event, index) {
        event.preventDefault();
        this.props.selectCardType(event.currentTarget.name);
        this.setState({ activeIndex: index });
    }

    render(){
        return (
            <div className="col-2 d-flex flex-column" id="shopCardsTypesWrap">
                <div className={this.state.activeIndex === 0 ? "shopcards-search selected" : "shopcards-search"}>
                    <a href="/" onClick={e => {this.handleClick(e, 0)}} name="dogs">Dogs</a>
                </div>
                <div className={this.state.activeIndex === 1 ? "shopcards-search selected" : "shopcards-search"}>
                    <a href="/" onClick={e => {this.handleClick(e, 1)}} name="cats">Cats</a>
                </div>
                <div className={this.state.activeIndex === 2 ? "shopcards-search selected" : "shopcards-search"}>
                    <a href="/" onClick={e => {this.handleClick(e, 2)}} name="birds">Birds</a>
                </div>
                <div className={this.state.activeIndex === 3 ? "shopcards-search selected" : "shopcards-search"}>
                    <a href="/" onClick={e => {this.handleClick(e, 3)}} name="reptiles">Reptiles</a>
                </div>
                <div className={this.state.activeIndex === 4 ? "shopcards-search selected" : "shopcards-search"}>
                    <a href="/" onClick={e => {this.handleClick(e, 4)}} name="fish">Fish</a>
                </div>
            </div>
        );
        
    }
}

export default ShopCardTypes;