import React from "react";

//Custom styles
import "../../../styles/shop/cards/shopCards.css";

function CardPack(props) {

    return (
        <div className="nes-container is-rounded animated bounceInRight card-pack">
            <p>Card Pack</p>
            <img src="images/card-sample.png" alt="card-sample" />
        </div>
    );
}

export default CardPack;