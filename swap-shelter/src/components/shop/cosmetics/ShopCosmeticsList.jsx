import React from "react";

//Custom styles
import "../../../styles/shop/cosmetics/shopCosmeticsList.css"

//Custom components
import ShopCosmeticItem from "./ShopCosmeticItem";

function ShopCosmeticsList(props) {

    const testImg = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEBUPExIPEBUQFRUWFRUVDw8WEBAWFRUWFhURFxUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGBAQFy0dFRkrLS0rKystLSstKy0rKysrLSsrLS0tNzctKy0tNy0tKzc3LTctKys3LSsrKysrLSsrK//AABEIAKgBLAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAAAQIEBQYDBwj/xAA/EAABAwIDBAgDBwMDBAMAAAABAAIDBBEFEiEGMUFREyIyQmFxgZEjUrEHFGJygqHBM5LRU2OiFTRD4URzdP/EABoBAAMBAQEBAAAAAAAAAAAAAAABAgMEBQb/xAAkEQEBAAICAgEEAwEAAAAAAAAAAQIRAyESMQQTIjJBBVFhcf/aAAwDAQACEQMRAD8A9UxxhmfHTjsk5pPyjcPUqyaLCw0AVdgrjIZZz33lrfyt0VivO+TfuaYlSIQuVRUiVCAEJQEuVXjx5UiAJcqUlMJV6wx/2gpTXHiUqrtopC2llcODSouewsAeKFxox8Nn5W/QLsoMqRKkKQCj11I2aN0TxdrwQVJSWTl0Hj2EVD8GxF0D79FIbeFidHL2CN4cA4agi49VkftF2d+90/SMHxYtQeJHEKv+z3atrqcwTOs+nHE6uaOHmt8sfObhNhi1WWNDGaySaNHLm70VPQUQlkEY1ihN3n/Wl8eYCYaiSQ5gPi1GkY/0o/nPJXcEYpow0NJDRqRqSeJKr8Mf9CeAlUOkxOGXRrxf5To72KmLmuzCEISCJV0gdqN6gMnfGbK5UarpQ4eKvHJUriZWytynQrM43Rva4TsHxI94+dvEK2kaWnkQmyvLt62xXITB8SDmh43O3+HMLQscCLhedVVR9xmzEHoJjr/tuPHyWuwvEBYC92u3FTnjssoNqICYekb2oSHj03j2UIVIlAkHeAK0UrA5padxBHusfhYyh8J0MLy303hTxliro2566XwiDfdaXYGY/djEe1C9zT5XuFn8Gbmmnk5vyjyCvMDd0dU5vCdlx+Zu/wDZdnHlqjknTWhIhqLLrcyBhdN0UDGcmi/mdSuy7OGllyXm/K/JpiEICeGrLHC5ejMCeGpdAmly28MeP8u6RxKYXJELLPlt9ej0FX12JdHNFFv6UkeVhdTybLIbQzEOZN/pyNPoTYqJNnpsFVbUtvRzD8BVmx1wDzF1yros8b2fM0j9kv2GcwXHDG1kc+gIblkHZOmgdyWoa4EXFiDx4LFUEeaENcL2u03/AAmy70dRLS9i8kfGMnrN/Kf4W2XFvuL8ettghQcOxSKcXY7Xi06Ob6KcsLLEkSoQkRrgvNJdn6dldNWXtDD1nDcHP+VbrGKsgCFh+JLpf5G8XrLYhhBrY308biyKEHUf+WXxPELr+PNTd9FV1si8TRmrNiZSbfgYNzVb1mIQxDrva3wJ19l5Fshj1TSPfh+XrvdlYXHRjufkvQqLZUWzTSOle7VxWfLO9/o4rcZxTD39a7gfmDHg+640GPOFhDUskA7km/yutGdlqQ7483m4rm7Y6g4QMB5i4PupmWJmU+01tJons/E3rM/ZW9LiUMvYkY71F/ZZ2TZmSHWCV1vkf1h5KIaQOdllhyO4ObcfuNyrwlVrbboWRjinj/pzvFu6/rNUxmOTx/1Ig8fNGf4Ki8dLxXNVSh48VUSNsbFTKLH6eU2D8rvld1T+6kVcAeLjf9U5bPZys7iVE2eJ0Tu8PY8CqHC6t1PoQcjTleOMbuDh+ErVObbQqmr2COZshHVl6j+XgVt1VtZhlYJGjW+m/mqOuj6OtdymYHerdCq2mmdRSgamF50P+mTwPgrXH5Rmp5huLi0/qCzk1U6NggDAQOJJ9SmVziwMnH/gkaT+V2hUgLoyESNfEe+wj14LSXVVZ000Lw4Bw3EXHquyo9lKkvp2tPaiux3m3RXa78buOSzshC55F1TXFRyYY3vISmhoCa5yRzkxcefNJ1iuQpKEiVctqghIlQEaukszzWN2gPSAU7T1pN/4QOK0+LyagcllcP8AiSyT8CcrfILfijTGNRs1V9JTtv2o+o7nduitVmMKBpp7nRlT7NeP8rTqOSaqb7UNFC1k0sBG852+Tt66z4dxajGfhSR1PAHI/wDK7cfdWYXTx2WKxyZeqw7XNZzHDc5uh/8Aa602MVEOkjenYO83R4HiOK0TmA7wo8tAw+HknlhKfVPw/FIZx1Hgni06OHou1bVNiYXu3D3J4AKlqdn2OOYEtcNzgbOHqFV1kVVFMzpBJVRNF2htrh/DNzCy+h2iz+k17XnS/wAep3/7Uf8AGi0FFSMhjEbRo0e/MlZzB8RDC6WojlikfxLSWtbwaCNyv6fEoJOzIw/qF0ua2dROnn32nbOuaRiENw5pGe3Dk5anYfaBtbTNN/iM0ePHmryrhbKx0brOa8EHyK8hoZX4PiZY64ikNvAtJ0PojH78dX3A9luhcoZ2uaHgghwuDfRK6oYN7mj9QXP42G6LnJC128BRZsZp275Y/wC6/wBFFdtHT7m9I/8ALG4qpMgly4e07tFFkw9w3WKb/wBZkd2KaU/ms0Jjpq5+5kMf5nFxWuMyVLUapw4O7cYPjbX3UNsEsJvDK4fgf1mHw8Fafdq52+eJvgIr/VUkH3qVry6ptkkcwWhZ3eKq4/2pIfjWtpozE75hrG714JK1jZonAEG4uCDxGoUN2HVB/wDkk+cTLKKzCqmF2eORjubC2zXf4KchrCnLZ6frC92lrhyIVPT1Un3Z0Em9lpInfM1rt3nZTsFlc4TNLCzKS6x4XGq5S0Zmo2lujw0lp/hPQX0T7tB5gFc8KrD95fGe7YjyKi4FKX07Cd4Fj5jRca93Q1Ec/B3Uf69kpWdG0GEu6Ktmh4TNEjfMaOWlBWTxiTI+CrHcdld+V+i1TTcX5rq4LvFzZ+yly5PcqnaXGRSw595JAaOdyrCF+ZodzAPuFy83Nb/wSOiQlKolbJly+a5faktCQG+qVFAQUJrzYEpBmtp6nKyR3G1h5nQKNglLlZHHyAv58VH2gdndGz55LnybqkkxUslEEYzSOHDXIF28U6bYzpdbQTQiEsc4A72gauDhuKr8J22jdGBIyXO3qus0u1HGwUmj2eB60xLydcoOg8zxXaqw4QvFRCwAtFntA7bf8hPkw2mxLFXBVxuiDr5huIIcPGxTMDqCWGJ/bhOV3iBud7KdTOilAlaGnxsLjwVZizOglFWOybNlA5cHeiw4743SYt0JGuBFxqCnLsUSyVCEwaWjzUSfCoH6uiZ6Cx/ZTUJWQlWMDhGrTKz8srrKn2l2LZWNHxJA9u4uNxbktWlS8YNKTDtm4YomxHO/KN5kfr6XUj/oNNxjB83OP8qzQjwgQ4sNgb2Yox+kKS2MDcAPIBPQn4wEsiyVIgBZfDuzL/8Aol+q1CzFANJvCokUZ+jjukSoWMWCwZJHW3Md9FCwkfAjH4QrCTSnmd+A/RQsNHwmflCNh1paZrLhugcb25E77JmMUWdjozxGnnwKkBTpY+kjDhvCLQp8Gm+9ULondpoLTzzN3fRaPZuq6WmjcTqBlPm3QrH4bL92ri3cyoFxyDxvCdVYm+ilkhbfK55e3ycB/IK04svFjnNpmOR/eZZzvbTROA5ZyL/RaPCJA6njdzY36KFs9RH7q5zu1UZnnn1r2HsjZN96ZreMZcw/pKz5cNccJcKvxU7lYKvxXgubH2cSKGTM3yUlVGHy2dbmra6rOCwq41brMPkut1GxE/DKmexGMr3XqR/txl3qVb7J4WI2dO4Xkm1JO8Dg0Koj1rJPBjR+62VK2zQPBd+Hpr6jqAhKhXpKtnpHRu6aHee0zuv8fArvT1cVQ1zNxsQ9ju0PRSlDrMObIQ8EseNz27/I8wsc+PfcTpHwp5iJpn72axn5mcPUK1VBWyPAAmGRzDdkzRdn6hvCm4PijZwRoHs7QB/5DwV4b/alkhCFoQQhCDCEIQAhI421UOlxJkrnNZdwZvf3L8geKAmpC5ZbaPbempOqD0r/AJW8PMrzrFttK2rdkaSxp3NZfMgPW8Rx+lgHxJWN8L3Pss7VfaVRtNmiR/kLBYjDtiK+p6zhkB70hN/ZaWg+zBg1lmLvBosEg3WE4g2oibM0EB4uAVT0Q/r/AP3u+itcGwtlLEIWZso3XN1WxaSVDPxtd/cFnyejhyEJFlFH1xtRzO/CVAw2djo2BrmmzRuIPBTse6tDJ4gD3IUZ+AQSRAw/CkY0Wc3S5txHFLCbJ3VnhjtC1Z/C6t0jSHiz4zlePEcVbUMmV48U85oVX7UYWXMzN0cw5mHxHBNgp2VsbJza+UNPgWk3C088QeLFYfEMLqYZHNgcWscc1uAJ3/RThkh6JEzK0NG4AD2VHgzeinng/F0jfJyv7KixodDPFU90no3+TuyfddnNhvBjKtlX4qNysFDxQdRebPbSe1Sw2N1e08mZoKorKdhs9jlPFa5zpplOllKSGkjUgGyq5K0TQB44mxHIjeFbLK4gDTzlm6KoNweDX8R6rLGdontW0WtXN+gLaxjQeSxeG/8AdTebPotqzcF34zppTkIQrSEIQgiOaCLHX6KrdgUQkE0d4nji3suHEEK0QloAJUiVMwhCEAIQhAVeL0z5bMzZIt7yD1nD5fALzvazbEi9HSDI1vVLm7z4NXpuKxOfDI1vac0geZCzGymw8VNaWW0kp117LPJIMZs7sHUVJ6SYmJh1ue25elYPs1S0oAjjbfi4i7j6q5AQSgEASrm99lxdOBqkqY2pKz84tVyj542H2NlciXWyp8R/7th+eJ49tVOXoa0EoCRPhFyB4rAzNrBaiI5lg/5BFA8tcPRJtwbUoHOSMfuFzjkazruNg0XJ8lXCnHtGhA+91Nt2ZvvbVT2lVmD3f0k5FumeXDy3BWSeSl7SyZmAp5YCoGFy72qxsuXLqs6nWUTFKMTROjPeGngeB91MSOXs3uacylwSrMkeV2j4jkePEcfVSK5l2Hw1Vfiw+7TtqgOpJZko4D5Xq10cOYIXl8uHhm2xrNMqml5jv1mgEjwK6g2WaxFkjMQMjQSGR3e0cW31PorDF5nmAyxO1bZ45OA4LTW4222FFOHt8QuWLUDZ4jGdOIPFpG4rN4Djee2YGN5AJY7S45jmFrIJg8XCxyxuN2zs1WBwaR33qVjwWvblB8bd4eC3rDoFRbSUOVwq2DrM0kA7zP8A0rmkkD2Nc03DgCCurjz3F2uyEJr3gak2WhHJr3gC50AVJiW1tFT6PlaSODTcrN132m0w0ZG+Tz0CNhqpdoqdptmcfJjip1HWMmbmZe3i0g/uvLaj7TJe5BG3zTaP7SqrMM0cZbfUAG/ogPXEqhYTiDamJsrbgO4EWIPJTEAqEiEEVCEJgIQhBkTXpya9TREKrdpbmo5aS63BqkujJffgNycIknRjlJHGnF3E8tFAxkWqKd3Nzm+4VwyOyq9pBYRP+SVn7myL6RndubhwXal7Q80VTbPPmnUQ64XPUoO3zwKdl9B00f1VM4OrHWFxCw6n/VI4DwV5ttC2SOJjhcOmbce6SOMNAaAABuA3I4r0nFX0kxFTJF3QxpaOA4FWiqK74dTFJweCw/wrZquqdqZ+VwKvWm4us6Fa0lUMtjwWOc2mxeJCUFIvVcjlUwNkaWOAIcLEKiw9zoH/AHWQ6b4nnvN+TzC0KiYlh7Z2Fh0O9rhvYeBCy5ePzisbpQT04biMbyNJY3t8Cd9lCraL7rIYz/RmvkPBjjvYfBJi9dJB0fTgh8MjbPA6krToT4Gy1dXSx1EWRwu14uP4IWfHhfHVa+TNYTRRVURgkHXpyQ1w0e0HskFV8GI1NHO6CQGVrdWuA6zm87cUrah9BXRtlByy/D6TuvHdJ/EFptoMO6VgkZ/Ui1afmHFvqqy49weQosWgqG2a9pJGrTo4eBBUGkP3SboCfhSm8R+R3GP/AAqxtPDMA/KL8baOaeIXKsw6RzcjZngAggO1ykbiCuWY+NXI2FQ8tYSBcgEgczyXiO0W09ZUyOYS5gBIyNvpbgvXcFrzLHlfpJH1Xjn+IeBUZuB0sLnTNibmcSSSLm5W8qpNvI8O2RrKjURuaD3n6LT4d9mh0Mso8mhehtdpfclgkuLpr8FJQbD0MYHws55uKuqbBaaPswxj9IXaCW6khCMpoMaALAADwCVCE0BCAmTzNYMznNaOZIAQD0XWdrdq4gcsTXTHmNGf3FUtVi9ZKe2IRyYLn3Q0nHa3TngbyB6pGytO4g+RC85khc/V75H+bymspMurHPYeBDiltf0K9LQQqDZjGHTAxSaSR/8AMfMFfpsrNUwtQGp6VA2YGqp2pjvSvPy2d7G6uFn9ssXip6Z4edZGkNHEkhTYHWU5g1/ztaf2XbDh11V4DOZaGCQ78tj6K2wvtLmyNB2xNmQnlO3+U0lRPtNkLKMPG9kjSPQrFUm3r26SRh3iDqtODiyzx3ETKT21+OwF8JI7UZDx6KZRzZ2NeO8AVUYTtJBVdUHK4jsnefJSMCdlzwHfE7T8rtQncbj1V72tU8OTEqim1xQE6yLL0XERKiyLJhwq6RkrSx7Q5p3ghOggDGhg3NFh5LshI9oGKYZHURmORoIO7m08CPFPoYHsjDHHMW6X5gbrqWUhRYNshi1MIKi40ZUeweP8oKvNoKDp4HNHaHWYeThqFmcOqTLGHEWcCWuHJw3hcvNjrt0ceW5oTSGGRs43CzXjm08fRXk7cwFtx1VTPEHtLT3hZTsAlL4G33suw/p0UY1rOu0iSIkWTXtIbYKXlQWK1eblTssLKS1Na1PCbPK7OQkSqkqHa3aAUUQdbM5+jRwv4rzujr56+V0kziWt3MGjB4WWo+1eAmCN43Mdr6qk2bo7RNA3u1KVa8OO8k+OO2gCmQUbneAU6mpA3fqVKUbd8iJHQtG/VdPurOQXdCRq2paIJI526ZXBrvFrjZbMFY7GxdjWje57AP7lsGDQeQVxw8/5FQlVFtPtLDQxlzyC49lgOrimwk2k4/jcVHEZZCNNw4uPILwbaTHZayYyvJt3W8GjkjaLH5q2UySHTutv1WhU6HVx8XXb2j7NqrpaAs4xErU4Z2l539j9YM0sB7wuF6NRMyyELl5Pdc+ftSfaa2+Hv8C36rxNxXuv2gRZqCUchf2K8KK9L+Lv21y8jtRzFj2vBILSCvUunyyQ1HdlaGP8zuK8mBXqOAEVFC1vEC3kRuV/Nw12virTJVBwer6SPXRzOq4cbhTl5jobFCEi9BxFSJUiYKhIlQDSEFKkKAQhZGpp+hq3M3NqRnbyDx2h6rXql2moTJFnb24TnYfLePULLkm8V4XVVZCTA35J5Ij3+u3+V2e4SMbM3c8X8jxCgVMTrtlZ24zcfiHFq5MK6dtQEtlEw7EGTNuNCO009pp5WUxbRNCEJUyCUJpSphX47hzamB8J7wNvA8FmcEhyMyOFnx9Vw4i3HyW2VVi2E5z0kZDJBx4O8ClWvFn41CQozakg5JGmN3j2T5FSbqNO+ZyiyWyc2MngVNgouJRInLlkQ6bDi+VsjuzHqBzceKvHuAFyQAFWV2MxQ9QfEfwY3U+vJZ+ubNVX6ZxYzhGw2H6jxV6cOeVzu3Da37QYqe8UFpJN1+41eYyR1dfKX2fK53sPJaDD8JgbNLGY3Tyh9mN1yAcC4rY4ZhIpgZ5XDMBubpHGOQHEqpDmUxeS4pgdTT2MsZaDx3hPw7CMw6WV3RR8z2neDQt7tNiDXRiWYdQm8MPek5PfyCwFbWPldmcfIDstHIBbcfDciy+TZNNTsRXxtr4hGzo2G7dT1nX4lewCO0oPML56wqqMUzJPkcD+6+iKOUSMbIO8AR6hcnzeL6dlYTPyQdpoc9JM3mx30XzxK6y+lq2PNG5vNpH7L56OFGWaRl8ojLsxO4AFP4HL4bg8PJTGpWm2O2kNO/I43jdvHynmsvLS6mxv/K4su0r1M/vx7ZT7a9ukd0cgqWaskAEgH7PVyx1wCNQVmdhKh0lGM2uW7fMLSQxhgyjQfRePnjquuVtUiVC73GEiEIASoQigFNQhAICkcLoQpOM1RxZHy0p4HpI/yu3j3XB7baFKhcGfWTow9Ik9IHHO1zo3jvNP1HFdI8aqYh8SMTAd5hs7+1KhXjV12o9rKeRua0jB4sPDep0OO0rzYTR3/MAUIVylpJbXRHdJGf1tXUTN+ZvuEIVFIXpW8x7hNdUM+Zv9wQhNXijTVtP3nxermqBPtBQs0MsZPJupPhohCQ9OD9oL/wBKB7vF1mhcJJ6iXtv6MfLH/LkIQqdmtjZE0nRvNx3nzKr+kmqjlhHRx7jK4an8oQhOFVvhuFRU4swau7Tzq93iSou0Qz9FDxkkF/JupQhaRlXl22mI9JVyG9ww5WjgANFnGSlzvBCF3cXqMM0tq9y+zzEOmomXNzH1T6bkIXN/JYy8ez4/bTv3Lwnal/QzTwDQukJceY4BCF53wZvNrLZtmk10YKRC+guM05t9vVthnM+6MDSLi+bwKlugqahznxOysBLR423n90IXi8mP312Y3p//2Q==";

    const cosmeticData = [
        {
            name: "Cool board",
            description: "This board is really cool!",
            type: "board",
            price: "200",
            img: testImg
        },
        {
            name: "Cool card",
            description: "This card is really cool!",
            type: "cardback",
            price: "100",
            img: testImg
        },
        {
            name: "Cool sound",
            description: "This sound is really cool!",
            type: "sfx",
            price: "375",
            img: testImg
        },
        {
            name: "Cool board",
            description: "This board is pretty bad :(",
            type: "board",
            price: "10",
            img: testImg
        },
        {
            name: "Cool profile pic",
            description: "This profile pic is alright.",
            type: "profilepic",
            price: "125",
            img: testImg
        },
        {
            name: "Coolest sound",
            description: "You best be willing to spend it all!",
            type: "sfx",
            price: "9999",
            img: testImg
        },
        {
            name: "Big card",
            description: "You gon card good boi!",
            type: "sfx",
            price: "6969",
            img: testImg
        },
        {
            name: "Budget board",
            description: "Financially impaired people!",
            type: "board",
            price: "10",
            img: testImg
        }
    ];

    const cosmeticsItems = [];

    function applySearchTerm(searchTerm) {
        const tempArr = [];
        if(searchTerm === "all") return cosmeticData;
        for(let i = 0; i < cosmeticData.length; i++) {
            cosmeticData[i].type === searchTerm && tempArr.push(cosmeticData[i]);
        }
        return tempArr;
    }

    (function createRows() {
        const modifiedData = applySearchTerm(props.cosmeticsType);

        modifiedData.forEach((data, i) => {
            cosmeticsItems.push(<ShopCosmeticItem key={i} {...data} />)
        })

        // for (let i = 0; i <= rowAmount; i++) {
        //     let itemAmount = Math.trunc(modifiedData.length / itemsPerRow) === 0 ? modifiedData.length : itemsPerRow;
        //     modifiedData.length > itemsPerRow && rowAmount++;
        //     cosmeticsItems.push(<ShopCosmeticRow key={i} rowId={i} itemAmount={itemAmount} data={modifiedData.splice(0, itemAmount)} />);
        // }
    }());

    return (
        <div className="col cosmetic-list">
            <div className="cosmetic-list-inner">
                {cosmeticsItems}
            </div>
        </div>
    );
}

export default ShopCosmeticsList;