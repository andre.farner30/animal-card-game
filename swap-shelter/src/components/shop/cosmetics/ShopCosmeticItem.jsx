import React from "react";

//Custom styles
import "../../../styles/shop/cosmetics/shopCosmeticItem.css"

function ShopCosmeticItem(props) {
    function canSample() {
        return props.type === "sfx";
    }

    function handleClick(event) {
        event.preventDefault();
    }

    return (
        <div className="shopCosmeticItemContainer nes-container is-rounded">
            <div className="shopCosmeticImageWrap">
                <img className="shopCosmeticImg" src="images/cosmetic.png" alt="Cosmetic" />
                <img src="https://png.pngtree.com/png-clipart/20190516/original/pngtree-vector-chess-icon-png-image_4164709.jpg" alt="Type" className="shopCosmeticType" />
                {canSample() && <a href="/" onClick={handleClick}><img src="images/sound.png" alt="Sample" className="shopCosmeticSample" /></a>}
                <div className="shopCosmeticPrice">
                    <span>{props.price}</span>
                    <img src="images/treat.png" alt="Treats" />
                </div>
            </div>
            <div className="shopCosmeticName">{props.name}</div>
            <div className="shopCosmeticDescription">{props.description}</div>
        </div>
    );
}

export default ShopCosmeticItem;