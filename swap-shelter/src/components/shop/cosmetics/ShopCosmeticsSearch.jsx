import React from "react";

//Custom styles
import "../../../styles/shop/cosmetics/shopCosmeticsSearch.css";

class ShopCosmeticsSearch extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            activeIndex: 0
        }
        this.handleClick= this.handleClick.bind(this);
    }

    handleClick(event, index) {
        event.preventDefault();
        this.props.selectCosmeticsType(event.currentTarget.name);
        this.setState({ activeIndex: index });
    }

    render(){
        return (
            <div className="col-2 d-flex flex-column" id="shopCosmeticsSearchWrap">
                <div className={this.state.activeIndex === 0 ? "shopcosm-search selected" : "shopcosm-search"}>
                    <a href="/" onClick={e => {this.handleClick(e, 0)}} name="all">All</a>
                </div>
                <div className={this.state.activeIndex === 1 ? "shopcosm-search selected" : "shopcosm-search"}>
                    <a href="/" onClick={e => {this.handleClick(e, 1)}} name="cardback">Card Backs</a>
                </div>
                <div className={this.state.activeIndex === 2 ? "shopcosm-search selected" : "shopcosm-search"}>
                    <a href="/" onClick={e => {this.handleClick(e, 2)}} name="board">Boards</a>
                </div>
                <div className={this.state.activeIndex === 3 ? "shopcosm-search selected" : "shopcosm-search"}>
                    <a href="/" onClick={e => {this.handleClick(e, 3)}} name="profilepic">Profile Pictures</a>
                </div>
                <div className={this.state.activeIndex === 4 ? "shopcosm-search selected" : "shopcosm-search"}>
                    <a href="/" onClick={e => {this.handleClick(e, 4)}} name="sfx">Sound Effects</a>
                </div>
            </div>
        );
    }
}

export default ShopCosmeticsSearch;