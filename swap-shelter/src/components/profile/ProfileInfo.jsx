import React, { Component } from "react";
import { Link } from "react-router-dom";

//Custom styles
import "../../styles/profile/profileInfo.css";

class ProfileInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editable: false,
            stats: {}
        };
    }

    componentDidMount = () => {
        fetch("/requests/userstats/" + this.props.user.email)
            .then(res => { return res.json() })
            .then(result => {
                this.setState({
                    stats: result
                });
            });
    };

    editProfileInfo = () => {
        this.state.editable ? this.setState({ editable: false }) : this.setState({ editable: true })
    };

    handleChange = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value
        });
    };

    render() {
        return (
            <div className="row my-sm-4 mt-sm-5 about-player nes-container is-rounded with-title">
                <p className="title">Player</p>
                <div className="col player-left">
                    <div className="player-img">
                        <img src="images/profilepic.gif" alt="user" />
                    </div>
                    <div className="player-control">
                        <Link className="treats-button nes-btn is-primary" to="/purchase-treats"><span>Purchase Treats</span></Link>
                        <button className="edit-button nes-btn is-error" onClick={this.editProfileInfo}>
                            <i className="far fa-edit fa-lg"></i>
                        </button>
                    </div>
                </div>
                <div className="col player-right">
                    <section className="message -left">
                        <div className="nes-balloon from-left">
                            <h3>Hi! I'm <span className="nes-text is-error">{this.props.user.userName}</span></h3>
                        </div>
                    </section>
                    <div className="player-info">
                        {this.state.editable ?
                            <input type="email" value={this.state.email} id="email" placeholder="E-mail"
                                className="nes-input" onChange={this.handleChange}></input>
                            : <p className="nes-text" >{this.props.user.email}</p>
                        }
                        {/* {this.state.editable ?
                            <select
                                name="gender"
                                value={this.state.gender}
                                onChange={this.handleChange}
                            >
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select> :
                            <p className="nes-text" >Rank: 1</p>
                        } */}
                    </div>
                    <div className="player-stats">
                        <p>Games: {this.state.stats.games} / Wins: {this.state.stats.wins} / Losses: {this.state.stats.losses}</p>
                        <p>Cards: {this.state.stats.cards} / Treats: {this.props.user.treats}</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default ProfileInfo;