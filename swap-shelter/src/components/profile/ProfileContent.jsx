import React, { Component } from "react";

import ProfileTabs from './ProfileTabs';
import AchievementList from "./AchievementList";
import CardCollection from "./cardCollection/CardCollection";

class ProfileContent extends Component {
    constructor() {
        super();
        this.state = {
            tabKey: "achievements"
        };
    }

    changeTab = (newKey) => {
        this.setState({
            tabKey: newKey
        });
    };

    render() {
        return (
            <div className="profile-content">
                <ProfileTabs changeTab={this.changeTab} />
                <div className="pContent">
                    {this.state.tabKey === "achievements" ?
                        <AchievementList /> :
                        <CardCollection user={this.props.user} />
                    }
                </div>
            </div>
        );
    }
}

export default ProfileContent;