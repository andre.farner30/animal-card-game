import React from "react";

//Custom components
import Achievement from "../profile/Achievement";

function AchievementList() {
    return (
        <div className="row achievementList">
            <div className="col">
                <Achievement
                    achievementTitle={"Just starting off"}
                    achievementDescription={"Get 10 WINS!"}
                    achievementPointsValue={20}
                    achievementPointsDate={"20/12/2019"}
                    curValue={100}
                    maxValue={100}
                />
                <Achievement
                    achievementTitle={"Better than most"}
                    achievementDescription={"Get 100 WINS!"}
                    achievementPointsValue={20}
                    achievementPointsDate={"20/12/2019"}
                    curValue={66}
                    maxValue={100}
                />
                <Achievement
                    achievementTitle={"Grandmaster"}
                    achievementDescription={"Get 1000 WINS!"}
                    achievementPointsValue={20}
                    achievementPointsDate={"20/12/2019"}
                    curValue={22}
                    maxValue={100}
                />
                <Achievement
                    achievementTitle={"This is just the beginning"}
                    achievementDescription={"Get 1000 Treats!"}
                    achievementPointsValue={20}
                    achievementPointsDate={"20/12/2019"}
                    curValue={12}
                    maxValue={100}
                />
                <Achievement
                    achievementTitle={"That's a lot of treats"}
                    achievementDescription={"Get 10000 Treats!"}
                    achievementPointsValue={20}
                    achievementPointsDate={"20/12/2019"}
                    curValue={33}
                    maxValue={100}
                />
                <Achievement
                    achievementTitle={"Full jar"}
                    achievementDescription={"Get 100000 Treats!"}
                    achievementPointsValue={20}
                    achievementPointsDate={"20/12/2019"}
                    curValue={5}
                    maxValue={100}
                />
            </div>
            <div className="col">
                <Achievement
                    achievementTitle={"Getting a footing"}
                    achievementDescription={"Play 50 Matches!"}
                    achievementPointsValue={20}
                    achievementPointsDate={"20/12/2019"}
                    curValue={87}
                    maxValue={100}
                />
                <Achievement
                    achievementTitle={"Seasoned warrior"}
                    achievementDescription={"Play 500 Matches!"}
                    achievementPointsValue={20}
                    achievementPointsDate={"20/12/2019"}
                    curValue={25}
                    maxValue={100}
                />
                <Achievement
                    achievementTitle={"Practiced veteran"}
                    achievementDescription={"Play 5000 Matches!"}
                    achievementPointsValue={20}
                    achievementPointsDate={"20/12/2019"}
                    curValue={72}
                    maxValue={100}
                />
                <Achievement
                    achievementTitle={"Getting some variety"}
                    achievementDescription={"Own 25 cards!"}
                    achievementPointsValue={20}
                    achievementPointsDate={"20/12/2019"}
                    curValue={63}
                    maxValue={100}
                />
                <Achievement
                    achievementTitle={"You're getting there"}
                    achievementDescription={"Own 100 cards!"}
                    achievementPointsValue={20}
                    achievementPointsDate={"20/12/2019"}
                    curValue={63}
                    maxValue={100}
                />
                <Achievement
                    achievementTitle={"Completionist"}
                    achievementDescription={"Own 500 cards!"}
                    achievementPointsValue={20}
                    achievementPointsDate={"20/12/2019"}
                    curValue={63}
                    maxValue={100}
                />
            </div>
        </div>
    );
}

export default AchievementList;