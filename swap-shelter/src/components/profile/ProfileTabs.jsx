import React from "react";

function ProfileTabs(props) {

    function handleClick(event) {
        event.preventDefault();
        props.changeTab(event.currentTarget.name);
    }

    return (
        <div className="row pTabs mt-lg-5" >
            <div className="col nes-container profile-tab">
                <a href="/" onClick={handleClick} className="col" id="achievementsTab" name="achievements">Achievements</a>
            </div>
            <div className="col nes-container profile-tab">
                <a href="/" onClick={handleClick} className="col" id="cardCollectionTab" name="cards">Cards</a>
            </div>
        </div>
    );
}

export default ProfileTabs;