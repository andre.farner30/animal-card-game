import React from "react";

//Custom styles
import '../../styles/profile/achievement.css';

//External styles
import 'react-circular-progressbar/dist/styles.css';

/*External libraries*/
//Circular Progress Bar docs - https://www.npmjs.com/package/react-circular-progressbar
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';

function Achievement(props) {
    return (
        <div className={props.curValue === props.maxValue ? "row achievement nes-container is-rounded isCompleted" : "row achievement nes-container is-rounded"}>
            <div className="col achievement-left">
                <div className="achievement-icon">
                    <img src="images/achievement.png" alt="Achievement icon" />
                </div>
                <div className="achievement-text">
                    <h3 className="achievement-title">
                        {props.achievementTitle}
                    </h3>
                    <div className="achievement-description">
                        {props.achievementDescription}
                    </div>
                </div>
            </div>
            <div className="col achievement-right">
                <i className="nes-icon coin is-small"></i>
                <span className="achievement-points-value">{` ${props.achievementPointsValue}`}</span>
                {props.curValue === props.maxValue ?
                    <div className="achievement-date" >
                        <i className="nes-icon trophy is-medium"></i><br/>{props.achievementPointsDate}</div> :
                    <CircularProgressbar
                        className="progressBar"
                        value={Math.trunc(props.curValue / props.maxValue * 100)}
                        text={`${Math.trunc(props.curValue / props.maxValue * 100)}%`}
                        styles={buildStyles({
                            pathColor: `rgba(231, 110, 85, ${props.curValue / 100})`,
                            textColor: '#e76e55',
                            trailColor: '#d6d6d6',
                            backgroundColor: '#3e98c7',
                          })}
                    />
                }
            </div>
        </div>
    );
}

export default Achievement;