import React, { useState, useCallback, useEffect } from "react";

//Custom styles
import "../../../styles/profile/card-collection.css";

//Custom components
import CollectionWrapper from "./CollectionWrapper";
import Collection from './Collection';
import DeckList from "./DeckList";


//Constants/Logic
import {
    setCellItemInList,
    pushValueToArray
} from "../../../logic/PlayLogic";

/*External libraries*/
//React DnD: DnDProvider docs - https://react-dnd.github.io/react-dnd/docs/api/dnd-provider
import { DndProvider } from 'react-dnd';

//React DnD: HTML5 Backend docs - https://react-dnd.github.io/react-dnd/docs/backends/html5
import HTML5Backend from 'react-dnd-html5-backend';

//React DnD: Touch Backend docs - https://github.com/redonkulus/react-dnd-touch-backend
import TouchBackend from 'react-dnd-touch-backend';

//React Scroll To Bottom docs - https://www.npmjs.com/package/react-scroll-to-bottom
import ScrollToBottom from 'react-scroll-to-bottom';

function CardCollection(props) {

    //State

    const [colCards, setColCards] = useState([]);

    const [decks, setDecks] = useState([]);

    const [buildingDeck, setBuildingDeck] = useState(false);

    const [userCards, setUserCards] = useState();

    const [userDecks, setUserDecks] = useState();

    //Button events

    // const editProfileInfo = () => {};

    const newDeck = () => {
        setDecks(pushValueToArray(decks, { cards: null, name: "" }));
        setBuildingDeck(true);
    };

    const user = props.user;
    async function fetchUrl() {
        const response = await fetch("/requests/usercards/" + user.email)
        const resJson = await response.json();
        setUserCards(resJson);

        const decks = [];
        Object.keys(user.decks).forEach(e => {
            const hand = [];
            if (user.decks[e].cards.length !== 0) {
                user.decks[e].cards.forEach(card => {
                    let c = resJson.find(x => x.id + "" === card + "");
                    hand.push(c);
                })
            }
            hand.length !== 0 && decks.push({ cards: hand, name: user.decks[e].name });
        });
        setUserDecks(decks);
    }

    useEffect(() => {
        fetchUrl()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);


    //Drag and Drop logic

    const handleReturn = useCallback(
        (item) => {
            let deckId;
            let cellId;
            decks.forEach((deck, i) => {
                deck.cards.forEach((cell, j) => {
                    if (cell.lastDroppedItem && cell.lastDroppedItem.cardId === item.cardId) {
                        deckId = i;
                        cellId = j;
                    }
                });
            });
            setDecks(setCellItemInList(decks, deckId, cellId, null));
        },
        [decks]
    );

    //Check for mobile devices
    const isMobile = () => {
        if ('ontouchstart' in document.documentElement) {
            return TouchBackend;
        } else {
            return HTML5Backend;
        }
    };

    //Render

    return (
        <DndProvider backend={isMobile()}>
            <div className="row card-collection">
                <ScrollToBottom className="col deck-side">
                    <DeckList
                        deckData={userDecks}
                        decks={decks}
                        colCards={colCards}
                        setDecks={setDecks}
                        setColCards={setColCards}
                        buildingDeck={buildingDeck}
                        setBuildingDeck={setBuildingDeck}
                    />
                    {(decks.length < 5 && !buildingDeck) &&
                        <div className="col new-deck nes-container">
                            <button onClick={newDeck}>
                                <h4 className="nes-text is-error">New Deck</h4>
                                <i className="far fa-plus-square fa-2x nes-text is-error"></i>
                            </button>
                        </div>
                    }
                </ScrollToBottom>
                <CollectionWrapper onDrop={handleReturn}>
                    <Collection setColCards={setColCards} colCards={colCards} cardData={userCards} />
                </CollectionWrapper>
            </div>
        </DndProvider>
    );
}

export default CardCollection;