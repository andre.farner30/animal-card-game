import React, { useCallback, useState } from "react";

//Constants/Logic
import { ItemTypes } from "../../../logic/Constants";
import {
    setCellItemInList,
    setCardInList
} from "../../../logic/PlayLogic";

//Custom components
import DropCell from "../../DropCell";

//Immutability helper docs - https://www.npmjs.com/package/immutability-helper
import update from 'immutability-helper';

//Axios docs - https://www.npmjs.com/package/axios
import axios from "axios";

function Deck(props) {

    const [savedDeck, setSavedDeck] = useState({ cards: null, name: "" });

    const [deckName, setDeckName] = useState(!props.deck ? "" : props.deck.name);

    const [editable, setEditable] = useState(props.editable);

    //Drag and Drop logic

    const decks = props.decks;
    const setDecks = props.setDecks;
    const deckId = props.deckId;

    const handleDrop = useCallback(
        (index, item) => setDecks(setCellItemInList(decks, deckId, index, item)),
        [decks, setDecks, deckId]
    );

    //Component logic

    let cells = [];
    let deckCells;

    if (!decks[deckId] || !decks[deckId].cards) {
        for (let i = 0; i < 5; i++) {

            let lastDroppedItem;

            if (props.buildingDeck) {
                lastDroppedItem = null;
            } else {
                lastDroppedItem = {
                    cardId: props.deck.cards[i].id,
                    info: props.deck.cards[i],
                    owned: true,
                    draggable: false,
                    type: ItemTypes.DECK_CARD,
                    played: true
                }
            }
            cells.push({
                cellId: i,
                lastDroppedItem: lastDroppedItem
            });
        }

        setDecks(
            update(decks, {
                [deckId]: {
                    $set: { cards: cells, name: deckName }
                }
            })
        );

        deckCells = cells.map(cell => (

            <DropCell
                cellId={cell.cellId}
                lastDroppedItem={cell.lastDroppedItem}
                cardDraggable={editable}
                cardType={ItemTypes.DECK_CARD}
                onDrop={item => handleDrop(cell.cellId, item)}
                key={cell.cellId}
            />
        ));

    } else {
        if (decks[deckId].cards) {
            deckCells = decks[deckId].cards.map(cell => (
                <DropCell
                    cellId={cell.cellId}
                    lastDroppedItem={cell.lastDroppedItem}
                    cardDraggable={editable}
                    cardType={ItemTypes.DECK_CARD}
                    onDrop={item => handleDrop(cell.cellId, item)}
                    key={cell.cellId}
                />
            ));
        }
    }

    const checkSaveConditions = () => {
        let fullDeck = true;
        decks[deckId].cards.forEach(cell => {
            if (!cell.lastDroppedItem) {
                fullDeck = false;
            }
        });
        if (!deckName) {
            fullDeck = false;
        }
        return fullDeck;
    }

    //Button events

    const handleEdit = () => {
        setSavedDeck(decks[deckId]);
        setEditable(true);
    };

    const handleDelete = () => {
        if (decks.length > 1) {
            setDecks(setCardInList(decks, deckId, null).filter(Boolean));
            handleDeckDelete(deckId);
        } else {
            alert("Can't delete last deck");
        }
    };

    const handleSave = (e) => {
        setDecks(
            update(decks, {
                [deckId]: {
                    name: {
                        $set: deckName
                    }
                }
            })
        );
        const res = checkSaveConditions();
        if (res) {
            setEditable(false);
            props.setBuildingDeck(false);
            // alert("Send to db");
            handleDeckPost(deckId, props.decks[deckId].cards, deckName);
        } else {
            alert("Fill in all the slots");
        }
    };

    async function handleDeckPost(deckId, deck, deckName) {
        let cardArr = [];
        deck.forEach(e => {
            cardArr.push(e.lastDroppedItem.info.id);
        })
        await axios.post("/requests/newdeck/" + props.userEmail, {
            deckId: deckId + 1,
            deck: {
                cards: cardArr,
                name: deckName
            }
        }).then(function (response) {
            props.updateUser(response.data)
        });
    }

    async function handleDeckDelete(deckId) {
        await axios.post("/requests/deldeck/" + props.userEmail, {
            deckId: deckId + 1
        }).then(function (response) {
            props.updateUser(response.data)
        });
    }

    const handleCancel = () => {
        if (props.buildingDeck) {
            setDecks(setCardInList(decks, deckId, null).filter(Boolean));
            props.setBuildingDeck(false);
        } else {
            setDecks(setCardInList(decks, deckId, savedDeck));
            setDeckName(decks[deckId].name);
            setEditable(false);
        }
    };

    const handleChange = (e) => {
        setDeckName(e.target.value);
    };

    //Render

    return (
        <div>
            {!editable ?
                <div className="profile-deck nes-container is-rounded">
                    <h4 className="nes-text is-error">{props.decks[deckId] ? props.decks[deckId].name : deckName}</h4>
                    <div className="row deck-cards-static justify-content-center">
                        {deckCells}
                    </div>
                    <div className="row deck-control justify-content-center">
                        {!props.buildingDeck && <button className="edit-deck nes-btn is-primary" onClick={handleEdit}>Edit</button>}
                        {(!props.buildingDeck && props.decks.length > 1) && <button className="delete-deck nes-btn is-error" onClick={handleDelete}>Delete</button>}
                    </div>
                </div>
                :
                <div className="col deck-builder nes-container">
                    <input type="text" className="nes-input deck-name-input" name="deck-name" placeholder="Deck Name" value={deckName} onChange={handleChange} />
                    <div className="row deck-cards-static justify-content-center">
                        {deckCells}
                    </div>
                    <div className="row deck-control border border-dark">
                        <button className="save-deck nes-btn is-primary" onClick={handleSave}>Save</button>
                        <button className="cancel-changes nes-btn is-error" onClick={handleCancel}>Cancel</button>
                    </div>
                </div>
            }
        </div>
    );
}

export default Deck;