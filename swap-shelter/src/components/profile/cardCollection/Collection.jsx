import React from "react";

//Constants
import { ItemTypes } from "../../../logic/Constants";

//Custom components
import Card from "../../Card";

function Collection(props) {

    let cards = [];
    let cardCol;

    if (props.colCards.length === 0) {
        if (props.cardData) {
            cards = props.cardData.map((data, i) => {
                return {
                    cardId: i,
                    info: data
                };
            });

            props.setColCards(cards);

            cardCol = cards.map(card => (
                <Card
                    cardId={card.cardId}
                    info={card.info}
                    owned={true}
                    draggable={true}
                    played={false}
                    showValues={true}
                    type={ItemTypes.CARD}
                    key={card.cardId}
                />
            ));
        }

    } else {
        cardCol = props.colCards.map((card) => {
            if (card) {
                return (
                    <Card
                        cardId={card.cardId}
                        info={card.info}
                        owned={true}
                        draggable={true}
                        played={false}
                        showValues={true}
                        type={ItemTypes.CARD}
                        key={card.cardId}
                    />
                );
            }
            return null;
        });
    }

    return (
        <div className="row all-cards justify-content-center">
            {cardCol}
        </div>
    );
}

export default Collection;