import React from "react";

//Custom component
import Deck from "./Deck";

import { UserSessionContext }  from "../../login-register/CurrentUserContext";

function DeckList(props) {

    let deckList = [];

    if (!props.decks[0] && props.deckData) {
        props.deckData.forEach((deck, i) => {
            deckList.push(
                <UserSessionContext.Consumer key={i}>
                    {context => (context.currentUser &&
                        <Deck
                            deckId={i}
                            deck={deck}
                            decks={props.decks}
                            colCards={props.colCards}
                            setDecks={props.setDecks}
                            setColCards={props.setColCards}
                            editable={props.buildingDeck}
                            buildingDeck={props.buildingDeck}
                            setBuildingDeck={props.setBuildingDeck}
                            userEmail={context.currentUser.email}
                            updateUser={context.updateUser}
                            key={i}
                        />)}
                </UserSessionContext.Consumer>
            );
        });
    } else {
        props.decks.forEach((deck, i) => {
            deckList.push(
                <UserSessionContext.Consumer key={i}>
                    {context => (context.currentUser &&
                        <Deck
                            deckId={i}
                            deck={deck}
                            decks={props.decks}
                            colCards={props.colCards}
                            setDecks={props.setDecks}
                            setColCards={props.setColCards}
                            editable={props.buildingDeck}
                            buildingDeck={props.buildingDeck}
                            setBuildingDeck={props.setBuildingDeck}
                            userEmail={context.currentUser.email}
                            updateUser={context.updateUser}
                            key={i}
                        />)}
                </UserSessionContext.Consumer>
            );
        });
    }

    //Render

    return (
        <div>
            {deckList}
        </div>
    );
}

export default DeckList;