import React from "react";

//Constants
import { ItemTypes } from "../../../logic/Constants";

//React DnD docs - https://react-dnd.github.io/react-dnd/docs/overview
import { useDrop } from 'react-dnd';

function CollectionWrapper(props) {

    const [{ isActive }, drop] = useDrop({
        accept: ItemTypes.DECK_CARD,
        drop: props.onDrop,
        collect: monitor => ({
            isActive: !!monitor.canDrop() && !!monitor.isOver()
        })
    });

    const backgroundColor = isActive ? 'lightgreen' : 'none';

    return (
        <div ref={drop} style={{ background: backgroundColor }} className="col cards-side">
            {props.children}
        </div>
    );
}

export default CollectionWrapper;