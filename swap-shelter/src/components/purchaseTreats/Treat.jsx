import React from "react";

/*External libraries*/
//React Stripe Checkout docs - https://www.npmjs.com/package/react-stripe-checkout
//React Stripe Checkout usage tutorial - https://www.youtube.com/watch?v=lkA4rmo7W6k
import StripeCheckout from "react-stripe-checkout";

//Axios docs - https://www.npmjs.com/package/axios
import axios from "axios";

//React Toastify docs - https://www.npmjs.com/package/react-toastify
import { toast } from "react-toastify";

//External style
import "react-toastify/dist/ReactToastify.css";

toast.configure();

function Treat(props) {
    const stripePublishableKey = "pk_test_ruRYWyS7CVZWG643PCNjxbXz001v1hq4ZH";

    const [product] = React.useState({
        name: props.name,
        price: props.price,
        currency: "eur"
    });

    async function handleToken(token) {
        const response = await axios.post(window.location.href.split("/").splice(0, 3).join("/") + "/checkout", {
            token,
            product,
            userEmail: props.userEmail
        });
        const { status } = response.data;
        if (status === "success") {

            toast("Success! Check email for details", {
                type: "success",
                onClose: () => { props.updateUser(props.userEmail) }
            });
        } else {
            toast("Something went wrong", { type: "error" });
        }
    }

    return (
        <div className="treat">
            {/*Beautify here*/}
            <StripeCheckout
                stripeKey={stripePublishableKey}
                token={handleToken}
                label={`Buy ${product.name}`}
                amount={product.price * 100}
                name={product.name}
            >
                <button className={props.special ? "nes-btn is-error special-treat" : "nes-btn is-primary"} >Buy {product.price * 100} Treats!</button>
            </StripeCheckout>
        </div>
    );
}

export default Treat;