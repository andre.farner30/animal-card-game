import React from "react";

//Custom styles
import "../../styles/purchaseTreats/treat.css";

//Custom components
import Treat from "./Treat";

import { UserSessionContext } from '../login-register/CurrentUserContext';

function TreatList() {
     return (
          <UserSessionContext.Consumer>
               {context =>
                    <div className="row treatList justify-content-center my-sm-5">
                         <Treat
                              userEmail={context.currentUser.email}
                              updateUser={context.updateUser}
                              name="100 Treats"
                              price={1}
                              special={false}
                         />
                         <section className="message -left popular-treat">
                              <i className="fas fa-otter fa-4x"></i>
                              <div className="nes-balloon from-left">
                                   <Treat
                                        userEmail={context.currentUser.email}
                                        updateUser={context.updateUser}
                                        name="1000 Treats"
                                        price={10}
                                        special={true}
                                   />
                                   <p>This is currently a popular option!</p>
                              </div>
                         </section>
                         <Treat
                              userEmail={context.currentUser.email}
                              updateUser={context.updateUser}
                              name="10000 Treats"
                              price={100}
                              special={false}
                         />
                    </div>}
          </UserSessionContext.Consumer>
     );
}

export default TreatList;