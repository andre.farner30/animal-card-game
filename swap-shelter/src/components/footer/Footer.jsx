import React from "react";

import CreatorSection from './CreatorSection';
import TechSection from './TechSection';

const Footer = () => {
    return (
        <footer className="row">
            <CreatorSection />
            <TechSection />
        </footer>
    );
}

export default Footer;