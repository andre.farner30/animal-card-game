import React from "react";

const CreatorSection = () => {
    return (
        <div className="col">
            <div className="row justify-content-center">
                <h5>Platform developed as a college project.</h5>
            </div>
            <br />
            <div className="row justify-content-center">
                <div className="col text-center">
                    <div className="row nes-container devContainer">
                        <p>{"<"}PM{">"} Jorge Rocha </p>
                        <img src="images/jorge.png" alt="dev" />
                    </div>
                    <div className="row nes-container devContainer">
                        <p>André Viana</p>
                        <img src="images/andre.png" alt="dev" />
                    </div>
                </div>
                <div className="col">
                    <div className="row nes-container devContainer">
                        <p>David Reis</p>
                        <img src="images/david.png" alt="dev" />
                    </div>
                    <div className="row nes-container devContainer">
                        <p>Ihor Polishchuk</p>
                        <img src="images/ihor.png" alt="dev" />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CreatorSection;