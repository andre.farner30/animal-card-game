import React from "react";

const TechSection = () => {
    return (
        <div className="col">
            <div className="row justify-content-center">
                <h5>Tech used in project:</h5>
            </div>
            <br />
            <div className="row justify-content-center">
                <div className="col nes-container techContainer">
                    <p>React.js</p>
                    <img src="images/react.png" alt="tech1" />
                </div>
                <div className="col nes-container techContainer">
                    <p>Node.js</p>
                    <img src="images/node.png" alt="tech2" />
                </div>
                <div className="col nes-container techContainer">
                    <p>Firebase</p>
                    <img src="images/firebase.png" alt="tech3" />
                </div>
            </div>
        </div>
    );
}

export default TechSection;