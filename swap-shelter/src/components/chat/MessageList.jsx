import React from "react";
import { auth, db, firebase } from "../../firebase/firebase.js"
//Custom styles
import "../../styles/chat/roomTab.css";

//Custom components
import Message from "./Message";

//React Scroll To Bottom docs - https://www.npmjs.com/package/react-scroll-to-bottom
import ScrollToBottom from 'react-scroll-to-bottom';

class MessageList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: MessageList.defaultProps.currentUser,
      room: props.currentRoom,
      messages: [],
      sentMessage: '',
      sentImg: '',
      inputKey: Date.now()
    };

    this.getMessages = this.getMessages.bind(this);

  }

  getMessages() {

    this.setState({
      messages: []
    });

    var that = this;
    var query = db.collection('messages')
      .where('chatName', '==', that.props.currentRoom)
      .orderBy('timestamp', 'asc')
      .limit(12);

    // Start listening to the query.
    query.onSnapshot(function (snapshot) {
      snapshot.docChanges().forEach(function (change) {
        var message = change.doc.data();
        var messages = that.state.messages;
        if (change.type === 'removed') {
          //deleteElement(change.doc.id);
        } else if (change.type === 'added') {

          if (message.timestamp) {
            messages.push(message);
            that.setState({
              messages: messages
            });
          }

        } else if (change.type === 'modified') {

          var foundIndex = messages.findIndex(
            x => x.timestamp.seconds === message.timestamp.seconds &&
              x.timestamp.nanoseconds === message.timestamp.nanoseconds);

          if (foundIndex === -1) {
            messages.push(message);
            that.setState({
              messages: messages
            });
          } else {
            messages[foundIndex] = message;
            that.setState({
              messages: messages
            });
          }
        }
      });

    });
  }

  componentDidMount() {
    var that = this;
    auth.onAuthStateChanged(function (user) {
      if (user) {
        that.getMessages()
      }

    });
  }

  onSubmitMessage = event => {
    const { sentMessage } = this.state;
    event.preventDefault();
    if (sentMessage !== '') {
      db.collection('messages').add({
        name: getUserName(),
        text: sentMessage,
        profilePicUrl: getProfilePicUrl(),
        chatName: this.props.currentRoom,
        timestamp: firebase.firestore.FieldValue.serverTimestamp()
      }).catch(function (error) {
        console.error('Error writing new message to Firebase Database', error);
      });
    }
    this.setState({
      sentMessage: ''
    });
  };

  onSubmitImg = event => {
    const LOADING_IMAGE_URL = 'https://www.google.com/images/spin-32.gif?a';
    const { sentImg } = this.state;
    event.preventDefault();
    if (sentImg) {
      // TODO 9: Posts a new image as a message.
      // 1 - We add a message with a loading icon that will get updated with the shared image.
      db.collection('messages').add({
        name: getUserName(),
        imageUrl: LOADING_IMAGE_URL,
        profilePicUrl: getProfilePicUrl(),
        chatName: this.props.currentRoom,
        timestamp: firebase.firestore.FieldValue.serverTimestamp()
      }).then(function (messageRef) {
        // 2 - Upload the image to Cloud Storage.
        var filePath = auth.currentUser.uid + '/' + messageRef.id + '/' + sentImg.name;
        return firebase.storage().ref(filePath).put(sentImg).then(function (fileSnapshot) {
          // 3 - Generate a public URL for the file.
          return fileSnapshot.ref.getDownloadURL().then((url) => {
            // 4 - Update the chat message placeholder with the image's URL.
            return messageRef.update({
              imageUrl: url,
              storageUri: fileSnapshot.metadata.fullPath
            })
          });
        });
      }).catch(function (error) {
        console.error('There was an error uploading a file to Cloud Storage:', error);
      });
    }
  };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  onChangeHandler = event => {
    this.setState({ [event.target.name]: event.target.files[0] });
  }

  componentDidUpdate(prevProps) {
    if (this.props.currentRoom !== prevProps.currentRoom) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
    {
      this.getMessages()
    }
  }

  render() {
    //this.setState({ room: this.props.currentRoom });    
    const { messages } = this.state;
    return (
      <div className="col-6 message-list" name={this.props.currentRoom}>
        <ScrollToBottom className="messages">
          {messages.map(function (message, i) {
            return <Message
              key={i}
              id={message.id}
              name={message.name}
              timestamp={message.timestamp}
              picURL={message.profilePicUrl}
              imageURL={message.imageUrl}
              message={message.text}
            />;
          })}
        </ScrollToBottom>
        <form id="message-form" onSubmit={this.onSubmitMessage}>
          <input type="text" className="nes-input" name="sentMessage" autoComplete="off" value={this.state.sentMessage} onChange={this.onChange} />
          <button id="submit" className="nes-btn is-primary" type="submit">Send</button>
        </form>
        <form id="image-form" onSubmit={this.onSubmitImg}>
          <input type="file" accept="image/*" key={this.state.inputKey} name="sentImg" onChange={this.onChangeHandler} />
          <button title="Add an image" className="nes-btn is-primary" type="submit" >
            <i className="material-icons">image</i>
          </button>
        </form>
      </div>
    );
  }
}

MessageList.defaultProps = {
  currentUser: null
};

function getProfilePicUrl() {
  // TODO 4: Return the user's profile pic URL.

  return auth.currentUser.photoURL || '/images/profile-small.jpg';
}

function getUserName() {
  // TODO 5: Return the user's display name.  
  return auth.currentUser.displayName || auth.currentUser.email;
}

export default MessageList;