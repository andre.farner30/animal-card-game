import React from "react";
import { auth, db, firebase } from "../../firebase/firebase.js"
//Custom styles
import "../../styles/chat/roomTab.css";

//Custom components
import UserDisplay from "./UserDisplay";

class ChatUsers extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      room: props.currentRoom,
      roomUsers: [],
    };

    this.getUsers = this.getUsers.bind(this);
  }

  getUsers() {
    this.setState({
      roomUsers: []
    });

    var that = this;
    var friendQuery = firebase.firestore()
      .collection('users')
      .where('rooms', 'array-contains', this.props.currentRoom).orderBy('userName');

    // Start listening to the query.
    friendQuery.onSnapshot(function (snapshot) {
      snapshot.docChanges().forEach(function (change) {
        if (change.type === 'added') {
          var users = that.state.roomUsers;
          users.push(change.doc.data().userName)
          that.setState({
            roomUsers: users
          });
        }
      });
    });
  }

  componentDidMount() {
    var that = this;
    auth.onAuthStateChanged(function (user) {
      if (user) {
        that.getUsers()
      }
    });
  }

  onSubmitMessage = event => {
    const { sentMessage } = this.state;
    event.preventDefault();
    if (sentMessage !== '') {
      db.collection('messages').add({
        name: getUserName(),
        text: sentMessage,
        profilePicUrl: getProfilePicUrl(),
        chatName: this.props.currentRoom,
        timestamp: firebase.firestore.FieldValue.serverTimestamp()
      }).catch(function (error) {
        console.error('Error writing new message to Firebase Database', error);
      });
    }
    this.setState({
      sentMessage: ''
    });
  };

  componentDidUpdate(prevProps) {
    if (this.props.currentRoom !== prevProps.currentRoom) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
    {
      this.getUsers()
    }
  }

  render() {
    //this.setState({ room: this.props.currentRoom });    
    const { roomUsers } = this.state;
    return (
      <div className="col-2 chat-users" name={this.props.currentRoom}>
        Chat Users
        {roomUsers.map(function (user) {
          return <UserDisplay
            key={user}
            name={user}
          />;
        })}
      </div>
    );
  }
}

ChatUsers.defaultProps = {
  currentUser: null
};

function getProfilePicUrl() {
  // TODO 4: Return the user's profile pic URL.
  return auth.currentUser.photoURL || '/images/profile-small.jpg';
}

function getUserName() {
  // TODO 5: Return the user's display name.  
  return auth.currentUser.displayName || auth.currentUser.email;
}

export default ChatUsers;