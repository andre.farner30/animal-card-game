import React from "react";

//Custom styles
import  "../../styles/chat/roomTab.css";


function Room(props) {

    const [room] = React.useState({
        name: props.name,
    });

    return (
        <div className='tab'>
            <div id={room.name} className='tablinks' >{room.name}</div> 
        </div>
    );
}

export default Room;