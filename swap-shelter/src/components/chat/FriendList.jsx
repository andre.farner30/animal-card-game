import React from "react";
import { auth, db, firebase } from "../../firebase/firebase.js"
//Custom styles
import "../../styles/chat/roomTab.css";
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

//Custom components
import UserDisplay from "./UserDisplay";

class MessageList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentUser: MessageList.defaultProps.currentUser,
            friends: [],
            friendInput: ''
        };

        this.getFriends = this.getFriends.bind(this);
        this.addFriendPopup = this.addFriendPopup.bind(this)
    }

    getFriends() {
        this.setState({
            friends: []
        });

        var that = this;
        var query = db.collection('users')
            .where('userName', '==', auth.currentUser.displayName);

        // Start listening to the query.
        query.onSnapshot(function (snapshot) {
            snapshot.docChanges().forEach(function (change) {
                that.setState({
                    friends: change.doc.data().friends
                });
            });
        });
    }

    componentDidMount() {
        var that = this;
        auth.onAuthStateChanged(function (user) {
            if (user) {
                that.getFriends()
            }
        });
    }

    onSubmitFriend = event => {
        const { friendInput, friends } = this.state;
        //event.preventDefault();
        if (friendInput !== '') {

            var friendQuery = firebase.firestore()
                .collection('users')
                .where('userName', '==', friendInput);

            var userExists = false;

            // Verificar se existe um user com esse username.
            friendQuery.get().then(function (querySnapshot) {
                if (querySnapshot.docs.length === 0) {
                    alert('No users with that name');
                } else if (querySnapshot.docs.length === 1) {
                    userExists = true;
                } else {
                    alert('Something went wrong with your friends..');
                }

                if (friendInput !== auth.currentUser.displayName) {
                    if (userExists && !friends.includes(friendInput)) {
                        var currentFriend = friends;
                        currentFriend.push(friendInput);
                        db.collection("users").doc(auth.currentUser.email).update({ friends: currentFriend });
                    }
                } else {
                    alert("You can't add yourself to your friends.. Sorry");
                }
            });

            this.setState({
                friendInput: ''
            });
        };
    }

    onChangeHandler = event => {
        this.setState({ friendInput: event.target.value });
    }

    addFriendPopup() {
        var that = this;
        const options = {
            customUI: ({ onClose }) => {
                function wrapper() {
                    that.onSubmitFriend();
                    onClose();
                }
                return (
                    <div className="react-confirm-alert-body">
                        <h1>Add a new friend</h1>
                        Enter his username
                        <div>
                            <input type="text" onChange={this.onChangeHandler} />
                        </div>
                        <div className="react-confirm-alert-button-group">
                            <button onClick={wrapper}>Add friend</button>
                            <button onClick={onClose}>Cancel</button>
                        </div>
                    </div>)
            },
            closeOnEscape: true,
            closeOnClickOutside: true,
            willUnmount: () => { },
            onClickOutside: () => { },
            onKeypressEscape: () => { }
        };

        confirmAlert(options);
    }

    componentDidUpdate(prevProps) {
        if (this.props.currentRoom !== prevProps.currentRoom) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
        {
            this.getFriends()
        }
    }

    render() {
        const { friends } = this.state;

        return (
            <div className='col-2 friends-list'>
                <button className="nes-btn is-error" onClick={this.addFriendPopup}>Add Friend</button>
                {friends.map(function (friend) {
                    return <UserDisplay
                        key={friend}
                        name={friend}
                    />;
                })}
            </div>
        );
    }
}

MessageList.defaultProps = {
    currentUser: null
};

export default MessageList;