import React from "react";
import { auth, db } from "../../firebase/firebase.js"
//Custom styles
import "../../styles/chat/roomTab.css";

const LOADING_IMAGE_URL = 'https://www.google.com/images/spin-32.gif?a';

class Message extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: props.id,
      name: props.name,
      timestamp: props.timestamp,
      picURL: props.picURL,
      imageURL: props.imageURL,
      showImg: LOADING_IMAGE_URL,
      message: props.message
    };

    if (props.message) {
      this.state.showImg = undefined;
    }
  }

  handleImageLoaded() {
    var that = this
    auth.onAuthStateChanged(function (user) {
      if (user) {
        db.collection('messages')
          .where('timestamp', '==', that.state.timestamp).onSnapshot(function (snapshot) {
            snapshot.docChanges().forEach(function (change) {
              var message = change.doc.data();
              if (change.type === "modified") {
                that.setState({
                  id: change.doc.id,
                  name: message.name,
                  timestamp: message.timestamp,
                  picURL: message.profilePicUrl,
                  imageURL: message.imageUrl,
                  showImg: message.imageUrl,
                  message: message.text
                });
              } else if (change.type === "added") {
                that.setState({
                  id: change.doc.id,
                  name: message.name,
                  timestamp: message.timestamp,
                  picURL: message.profilePicUrl,
                  imageURL: message.imageUrl,
                  showImg: message.imageUrl,
                  message: message.text
                });
              }
            });
          });
      }
    });
  }

  handleImageErrored() {
    this.setState({ imageURL: "failed to load" });
  }

  render() {
    const iconStyle = {
      backgroundImage: 'url(' + addSizeToGoogleProfilePic(this.state.picURL) + ')'
    };

    return (
      <div className="message-container" id={this.state.timestamp}>
        <div className="spacing">
          <div className="pic" style={iconStyle}>
          </div>
        </div>
        <div className="message">
          {this.state.message}
          <img src={this.state.showImg}
            onLoad={this.handleImageLoaded.bind(this)}
            onError={this.handleImageErrored.bind(this)}
            alt=''
          />
        </div>
        <div className="name">
          {this.state.name}
        </div>
      </div>
    );
  }
}

function addSizeToGoogleProfilePic(url) {
  if (url.indexOf('googleusercontent.com') !== -1 && url.indexOf('?') === -1) {
    return url + '?sz=150';
  }
  return url;
}

export default Message;