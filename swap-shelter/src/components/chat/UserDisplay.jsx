import React from "react";
import { auth, db } from "../../firebase/firebase.js"
//Custom styles
import "../../styles/style.css";

class UserDisplay extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: props.name,
      picUrl: 'Getting pic',
    };
  }

  getFriend(){
    var that = this;
    var friendQuery = db.collection('users')
    .where('userName', '==', this.state.name);

    // Verificar se existe um user com esse username.
    friendQuery.get().then(function (querySnapshot) {
      if (querySnapshot.docs.length === 1) {
        querySnapshot.forEach(function (doc) {
          that.setState({
            picUrl: doc.data().photo
          });
        });
      } else {
        alert('Something went wrong with your friends..');
      }
    });
  }

  componentDidMount() {
    var that = this;
    auth.onAuthStateChanged(function (user) {
        if (user) {
            that.getFriend();
        }
    });
    }

  render() {
    const iconStyle = {
      backgroundImage: 'url(' + addSizeToGoogleProfilePic(this.state.picUrl) + ')'
    };

    return (
      <div className="message-container">
        <div className="spacing">
          <div className="pic" style={iconStyle}>
          </div>
        </div>
        <div className="message">
          {this.state.name}
        </div>
      </div>
    );
  }
}

function addSizeToGoogleProfilePic(url) {
  if (url.indexOf('googleusercontent.com') !== -1 && url.indexOf('?') === -1) {
    return url + '?sz=150';
  }
  return url;
}

export default UserDisplay;