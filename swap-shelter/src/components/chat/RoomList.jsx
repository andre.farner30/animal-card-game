import React from "react";
import { auth, db } from "../../firebase/firebase.js"
//Custom styles
import "../../styles/chat/roomTab.css";

//Custom components
import Room from "./Room";

class RoomList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentUser: RoomList.defaultProps.currentUser,
      rooms: [],
      currentRoom: props.currentRoom
    };
  }

  render() {
    return (
      <div id="tabs">
        {this.state.rooms.map(function (room) {
          return <Room key={room} name={room} />;
        })}
        <button>+</button>
      </div>
    );
  }
}

RoomList.defaultProps = {
  currentUser: null
};

export default RoomList;