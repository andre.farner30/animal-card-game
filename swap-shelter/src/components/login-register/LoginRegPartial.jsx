import React, { Component } from "react";
import { withRouter, Link } from 'react-router-dom';

import { auth } from '../../firebase/firebase';
import { UserSessionContext } from '../login-register/CurrentUserContext';

class LoginRegPartial extends Component {
    render() {
        const handleLogout = (context) => {
            auth.signOut();
            context.destroySession();
            if (window.location.pathname !== "/") window.location.pathname = "/";
        };
        return (
            <div className="loginPartial">
                <UserSessionContext.Consumer>
                    {context => (context.currentUser ?
                        <div className="row">
                            <div className="col userLogoutSection">
                                <Link to="/profile">{context.currentUser.userName}</Link>
                                <button type="button" className="nes-btn" onClick={() => handleLogout(context)}>Logout</button>
                            </div>
                            <div className="col log-partial-right">
                                <div className="profile-pic-small-container">
                                    <Link to="/profile">
                                        <img className="profile-pic-small nes-avatar is-rounded is-large"
                                            src="images/profile-small.jpg" alt="pp" />
                                    </Link>
                                </div>
                                <div className="row justify-content-center log-partial-treats">
                                    <img src="images/treat.png" alt="Treats" />
                                    <span>{context.currentUser.treats}</span>
                                </div>
                            </div>
                        </div>
                        :
                        <div className="loginSection">
                            <Link to="/login-register" id="loginregbutton" >Login/Register</Link>
                        </div>
                    )}
                </UserSessionContext.Consumer >
            </div>
        )
    }
};

export default withRouter(LoginRegPartial);