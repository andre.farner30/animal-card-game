import React from "react"

import { auth } from "../../firebase/firebase.js"
import { withRouter } from 'react-router-dom'

const INITIAL_STATE = {
    email: '',
    password: '',
    error: null,
    errorDisplay: "none"
};

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = { ...INITIAL_STATE };
    }

    onSubmit = event => {
        const { email, password } = this.state;
        event.preventDefault();
        auth.signInWithEmailAndPassword(email, password)
            .then(() => {
                this.setState({ ...INITIAL_STATE });
                this.props.history.push("/")
            })
            .catch(error => {
                this.setState({
                    error: error.message,
                    errorDisplay: "block"
                });
            });
    };

    onChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    render() {
        const { email, password } = this.state;
        return (
            <div>
                <div id="login-forma">
                    <h2>Login</h2>
                    <form id="login-form" onSubmit={this.onSubmit}>
                        <div className="nes-field loginfield">
                            <label htmlFor="inputEmaill" >E-mail</label>
                            <input type="text" className="nes-input form-control" id="inputEmaill" placeholder="Email"
                                spellCheck="false" name="email" value={email} onChange={this.onChange} />
                        </div>
                        <div className="nes-field loginfield">
                            <label htmlFor="inputPasswordl">Password</label>
                            <input type="password" className="nes-input form-control" id="inputPasswordl"
                                ng-model="password" placeholder="Password" name="password" value={password} onChange={this.onChange} />
                        </div>
                        <div className="form group">
                            <p className="nes-text is-error error errorMessage"
                                style={{ visibility: this.state.error === null ? "hidden" : "visible", display: "block" }} >
                                *{this.state.error}*
                            </p>
                        </div>
                        <button type="submit" className="nes-btn is-error" >Login</button>
                    </form>
                </div>
            </div>
        )
    }
}
export default withRouter(Login)