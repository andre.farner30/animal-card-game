import React from "react"
import { auth, db } from "../../firebase/firebase.js"
import { withRouter } from 'react-router-dom'

const INITIAL_STATE = {
    email: '',
    password: '',
    repassword: '',
    userName: '',
    error: null,
    errorDisplay: "none"
};

class Register extends React.Component {

    constructor(props) {
        super(props);

        this.state = { ...INITIAL_STATE };
        this.saveUsername = this.saveUsername.bind(this)
    }


    onSubmit = event => {
        const { email, password, repassword, userName } = this.state;
        event.preventDefault();
        if (password === repassword) {
            auth.createUserWithEmailAndPassword(email, password)
                .then((cred) => {
                    this.saveUsername(email, userName).then(() => {
                        this.setState({ ...INITIAL_STATE });
                        this.props.history.push("/")
                    });
                    return cred.user.updateProfile({
                        displayName: userName
                    });
                })
                .catch(error => {
                    this.setState({ error: error.message });
                });
        }
    };

    async saveUsername(email, userName) {
        let userNameL = "default";
        if (userName === "") {
            userNameL = email.substr(0, email.indexOf('@'));
        }
        else {
            userNameL = userName;
        }
        await db.collection('users').doc(email).set({
            email: email,
            userName: userNameL,
            treats: 1000,
            isAdmin: true,
            rooms: ['Global'],
            photo: '/images/profile-small.jpg',
            friends: [],
            decks: {
                deck1: {
                    name: "Default",
                    cards: [1, 2, 3, 4, 5]
                },
                deck2: {
                    name: "---",
                    cards: []
                },
                deck3: {
                    name: "---",
                    cards: []
                },
                deck4: {
                    name: "---",
                    cards: []
                },
                deck5: {
                    name: "---",
                    cards: []
                }
            }
        }).then(() => {
            db.collection('stats').doc("" + email).set({
                userName: userNameL,
                games: 0,
                wins: 0,
                losses: 0,
                cards: 5
            }).then(() => {
                db.collection('user-cards').doc("" + email).set({
                    userName: userNameL,
                    cards: [1, 2, 3, 4, 5]
                });
            });
        }).then(() => {
            this.setState({ userName: '' })
        }).catch(error => {
            this.setState({ error: error.message })
        })


    }

    onChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    render() {
        const { email, password, repassword, userName } = this.state;
        return (
            <div>
                <div id="register-forma">
                    <h2>Register</h2>
                    <form id="register-form" onSubmit={this.onSubmit} autoComplete="off">
                        <div className="nes-field registerfield">
                            <label htmlFor="inputEmailr" >E-mail</label>
                            <input type="text" className="nes-input form-control" id="inputEmailr" placeholder="Email"
                                spellCheck="false" name="email" value={email} onChange={this.onChange} />
                        </div>
                        <div className="nes-field registerfield">
                            <label htmlFor="inputUsernamer" >Username <span className="nes-text is-primary">(Optional)</span></label>
                            <input type="text" className="nes-input form-control" id="inputUsernamer" placeholder="Username"
                                spellCheck="false" name="userName" value={userName} onChange={this.onChange} />
                        </div>
                        <div className="nes-field registerfield">
                            <label htmlFor="inputPasswordr">Password
                                <span className="nes-text is-primary"> (6 characters or more)</span>
                            </label>
                            <input type="password" className="nes-input form-control" id="inputPasswordr"
                                ng-model="password" placeholder="Password" name="password" value={password} onChange={this.onChange} />
                        </div>
                        <div className="nes-field registerfield">
                            <label htmlFor="reinputPasswordr">Retype Password</label>
                            <input type="password" className="nes-input form-control" id="reinputPasswordr"
                                ng-model="password" placeholder="Retype Password" name="repassword" value={repassword} onChange={this.onChange} />
                        </div>
                        <p className="nes-text is-error error errorMessage"
                            style={{ visibility: this.state.error === null ? "hidden" : "visible", display: "block" }} >
                            *{this.state.error}*
                            </p>
                        <button type="submit" className="nes-btn is-error" >Register</button>
                    </form>
                </div>
            </div>
        )
    }

}

export default withRouter(Register)