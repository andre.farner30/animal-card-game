
import React, {
    Component,
    createContext
} from 'react';

import { auth, db } from '../../firebase/firebase.js';


export const UserSessionContext = createContext();

class CurrentUserContext extends Component {

    constructor(props) {
        super(props)
        this.state = {
            currentUser: CurrentUserContext.defaultProps.currentUser
        };
        this.updateUser = this.updateUser.bind(this);
    }

    componentDidMount() {
        auth.onAuthStateChanged(user => {
            if (user) {
                db.collection('users').doc(user.email).get().then(doc => {
                    if (doc.exists) {
                        let data = doc.data();
                        this.setState({
                            currentUser: data
                        });
                    }
                });
            }
        });
    }

    updateUser(userData) {
        db.collection('users').doc(userData).get().then(doc => {
            if (doc.exists) {
                let data = doc.data();
                this.setState({
                    currentUser: data
                });
            }
        });
    }

    render() {
        return (
            <UserSessionContext.Provider
                value={{
                    currentUser: this.state.currentUser,
                    updateUser: this.updateUser,
                    destroySession: () => this.setState({
                        currentUser: CurrentUserContext.defaultProps.currentUser
                    })
                }}
            >
                {this.props.children}
            </UserSessionContext.Provider>
        );
    }
}

CurrentUserContext.defaultProps = {
    currentUser: null
};

export default CurrentUserContext;
