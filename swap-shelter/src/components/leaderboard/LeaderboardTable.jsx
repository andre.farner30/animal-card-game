import React from "react";

//Custom styles
import "../../styles/leaderboard/leaderboardTable.css";

//External styles
import "react-table/react-table.css";

/*External libraries*/
//React Table docs - https://www.npmjs.com/package/react-table
import ReactTable from "react-table";

const columns = [
    {
        Header: () => (
            <span>
                <i className="fas fa-hashtag"></i>
            </span>
        ),
        id: "row",
        maxWidth: 100,
        filterable: false,
        Cell: row => {
            return <div>
                {row.index + 1}
            </div>;
        }
    },
    {
        Header: "Name",
        accessor: "userName"
    },
    {
        Header: "Wins",
        accessor: "wins",
        maxWidth: 150,
    },
    {
        Header: "Games",
        accessor: "games",
        maxWidth: 200
    },
    {
        Header: "Cards",
        accessor: "cards",
        maxWidth: 150
    }
    ,
    {
        Header: "Loses",
        accessor: "losses",
        maxWidth: 150
    }
];

class LeaderboardTable extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("/requests/stats")
            .then(res => { return res.json() })
            .then(result => {
                let sorted = result.sort((a, b) => (a.wins / a.games > b.wins / b.games) ? -1 : 1)
                sorted.forEach(e => {
                    this.setState({
                        items: [...this.state.items, e]
                    }
                    );
                })
            },
                error => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        return (
            <ReactTable className="-striped -highlight" defaultPageSize={10} data={this.state.items} columns={columns} filterable={true} style={{ border: "none" }} />
        );
    }
}

export default LeaderboardTable;