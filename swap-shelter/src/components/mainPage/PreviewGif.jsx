import React from 'react';

const PreviewGif = () => {
    return (
        <div className="col-2 mx-sm-4 nes-container is-rounded is-dark">
            <div className="game-gif">
                <img src="images/preview.gif" alt="example" />
            </div>
            <span>Preview</span>
        </div>

    )
}

export default PreviewGif;