import React from 'react';

import PreviewGif from './PreviewGif';

const PreviewRow = () => {
    return (
        <div className="row my-sm-4 justify-content-center">
            <PreviewGif />
            <PreviewGif />
            <PreviewGif />
        </div>
    )
}

export default PreviewRow;