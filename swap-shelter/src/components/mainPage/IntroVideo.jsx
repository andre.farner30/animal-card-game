import React from 'react';

const IntroVideo = () => {
    return (
        <div className="col my-sm-3">
            <section className="message -right">
                <div className="nes-balloon from-right" id="intro-video">
                    <p className="text-left"><i className="nes-icon youtube"></i> Watch this!</p>
                    <iframe title="intro" width="420" height="315" src="https://www.youtube.com/embed/tsDqKLqnutI" frameBorder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen>
                    </iframe>
                </div>
                <i className="fas fa-cat fa-5x align-bottom"></i>
            </section>
        </div>
    )
}

export default IntroVideo;