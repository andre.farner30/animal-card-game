import React from "react";

import IntroVideo from './IntroVideo';
import ButtonArea from './ButtonArea';
import PreviewRow from './PreviewRow';

class MainBody extends React.Component {
    render() {
        return (
            <div className="main">
                <div className="row my-sm-4 mt-sm-5 nes-container is-rounded with-title">
                    <p className="title">Introduction</p>
                    <br />
                    <IntroVideo />
                    <ButtonArea />
                </div>
                <div className="row text-justify my-sm-5 nes-container is-rounded with-title">
                    <p className="title">Description</p>
                    <p>Meet the newest entry to the web card game meta!</p>
                    <p><b className="nes-text is-error">Shelter Swap</b>, at its core, is a pretty simple game with the possibility to become
                                exponentially more complex with the use of modifiers.
                                The core gameplay loop consists of placing one of your animal cards each turn, with the goal
                                of capturing the opponents animal by positioning
                                your animal such that the side that makes contact with the enemy has a higher value than said enemy!</p>
                </div>
                <div className="row my-sm-4 nes-container is-rounded with-title">
                    <p className="title">Previews</p>
                    <br />
                    <div className="col ">
                        <PreviewRow />
                        <PreviewRow />
                    </div>
                </div>
            </div>
        );
    }
}

export default MainBody;