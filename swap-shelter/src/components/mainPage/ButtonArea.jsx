import React from 'react';
import { Link } from "react-router-dom";

const ButtonArea = () => {
    return (
        <div className="col-md my-sm-5">
            <div className="row justify-content-center py-lg-5">
                <Link id="go-play" className="nes-btn is-error playButton" to="/game">Play!</Link>
            </div>
            <hr id="buttonAreaLine" />
            <div className="row justify-content-center py-lg-5">
                <Link id="go-play" className="nes-btn is-primary l2Button" to="/l2play">Learn to Play!</Link>
            </div>
        </div>
    )
}

export default ButtonArea;