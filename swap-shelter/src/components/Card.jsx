import React, { useState } from 'react';

//Custom styles
import '../styles/card.css';

/*External libraries*/
//React DnD docs - https://react-dnd.github.io/react-dnd/docs/overview
//Similar example - https://react-dnd.github.io/react-dnd/examples/dustbin/multiple-targets
//React DnD: useDrag docs - https://react-dnd.github.io/react-dnd/docs/api/use-drag
import { useDrag } from 'react-dnd';

import ReactTooltip from 'react-tooltip';

function Card(props) {

    const [draggable, setDraggable] = useState(props.draggable);

    const [{ isDragging }, drag] = useDrag({
        item: { cardId: props.cardId, info: props.info, owned: props.owned, draggable: draggable, type: props.type, played: true},
        canDrag: () => draggable,
        collect: monitor => ({
            isDragging: monitor.isDragging()
        })
    });

    if (draggable !== props.draggable) {
        setDraggable(props.draggable);
    }
    
    const owned = props.owned ? " isOwned blue " : " red ";

    if(isDragging && props.hideSourceOnDrag){
        return <div ref={drag} className="dummyCard"/>
    }

    let elem;
    switch(props.info.elemental){
        case "X" : elem = null;
        break;
        case "cat" : elem = <img className="card-element" src="images/cat-element.png" alt="c-elem"/>
        break;
        case "dog" : elem = <img className="card-element" src="images/dog-element.png" alt="d-elem"/>
        break;
        default : elem = null;
    }

    let backgroundImage = ((props.played || props.owned) || (!props.owned && props.showValues)) ? "url(" + props.info.img + ")" : "";
    
    
    return (
        <div ref={drag} data-tip={props.info.name} data-for='card-name'
            className={owned + "gCard nes-container is-rounded"} style={{backgroundImage: backgroundImage}}>
            {((props.played || props.owned) || (!props.owned && props.showValues)) && <div>
                <div className="card-values">
                    <p>{props.info.up}</p>
                    <p>{props.info.left}{props.info.right}</p>
                    <p>{props.info.down}</p>
                </div>
                <div className="card-element">
                    {elem}
                </div>
            </div>}
            {!props.draggable && <ReactTooltip className='card-tooltip' id='card-name' type='dark' effect="solid" />}
        </div>
    );
}

export default Card;