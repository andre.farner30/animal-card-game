import React from 'react';
import ReactDOM from 'react-dom';

//Custom style
import './index.css';

//Custom components
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));