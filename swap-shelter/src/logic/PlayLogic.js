/*External libraries*/
//Immutability helper docs - https://www.npmjs.com/package/immutability-helper
import update from 'immutability-helper';

export const convertIdToCoords = (index) => {
    switch (index) {
        case 0: return [1, 1];
        case 1: return [1, 2];
        case 2: return [1, 3];
        case 3: return [2, 1];
        case 4: return [2, 2];
        case 5: return [2, 3];
        case 6: return [3, 1];
        case 7: return [3, 2];
        case 8: return [3, 3];
        default: return -1;
    }
}

export const convertCoordsToId = (coords) => {
    switch (coords[0]) {
        case 0:
            switch (coords[1]) {
                case 0: return 0;
                case 1: return 3;
                case 2: return 6;
                default: return -1;
            }
        case 1:
            switch (coords[1]) {
                case 0: return 1;
                case 1: return 4;
                case 2: return 7;
                default: return -1;
            }
        case 2:
            switch (coords[1]) {
                case 0: return 2;
                case 1: return 5;
                case 2: return 8;
                default: return -1;
            }
        default: return -1;
    }
}

export const setCellItem = (cells, index, item) => {
    return update(cells, {
        [index]: {
            lastDroppedItem: {
                $set: item
            }
        }
    });
};

export const setCellItemInList = (cells, index, cellIndex, item) => {
    return update(cells, {
        [index]: {
            cards: {
                [cellIndex]: {
                    lastDroppedItem: {
                        $set: item
                    }
                }
            }
        }
    });
};

export const setCardInList = (list, index, newValue) => {
    return update(list, {
        [index]: {
            $set: newValue
        }
    });
};

export const updateCardOwnership = (cells, index) => {
    return update(cells, {
        [index]: {
            lastDroppedItem: {
                owned: {
                    $apply: function (prevOwner) { return !prevOwner; }
                }
            }
        }
    });
};

export const pushValueToArray = (array, value) => {
    return update(array, {
        $push: [value]
    });
};

export const getRandomCardInHand = (cards) => {
    const cardsInHand = [];
    cards.forEach(card => {
        typeof card !== "string" && cardsInHand.push(card);
    });
    return cardsInHand[Math.floor(Math.random() * Math.floor(cardsInHand.length))];
}

export const getRandomEmptyCellId = (gameCells) => {
    const emptyCellsIds = [];
    gameCells.forEach(cell => {
        !cell.lastDroppedItem && emptyCellsIds.push(cell.cellId);
    });
    return emptyCellsIds[Math.floor(Math.random() * Math.floor(emptyCellsIds.length))];
}
