import React from "react";
import '../styles/404.css';

const NotFound = () => {
    return (
        <div id="message">
            <img src="images/404.png" alt="404"/>
            <h2>404</h2>
            <h1>Page Not Found</h1>
            <p>The specified page was not found on this website. Please check the URL for mistakes and try again.</p>
        </div>
    );
};

export default NotFound;