import React from "react";
import { Link } from "react-router-dom";
import '../styles/loginreg.css';
import Login from "../components/login-register/Login"
import Register from "../components/login-register/Register"

// const LoginRegister = () => {
class LoginRegister extends React.Component {

    render() {
        return (
            <div>
                <div className="loginreg-logo">
                    <Link to="/"><img src="images/ESWLogo.png" alt="logo" /></Link>
                </div>
                <div className="main">
                    <div className="row loginreg">
                        <div className="col">
                            < Login />
                        </div>
                        <i className="nes-octocat animate loginregicon"></i>
                        <div className="col">
                            < Register />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default LoginRegister;