import React from "react";
import { withRouter } from 'react-router-dom';
import { auth, db } from "../firebase/firebase.js"
import '../styles/chat/chat.css';
import FriendList from "../components/chat/FriendList"
import MessageList from "../components/chat/MessageList";
import ChatUsers from "../components/chat/ChatUsers";
import { UserSessionContext } from "../components/login-register/CurrentUserContext"
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

class Chat extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      currentRoom: 'Global',
      rooms: [],
      roomInput: '',
      friends: [],
      friendsInv: [],
      hidden: true
    };

    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onRoomInputChange = this.onRoomInputChange.bind(this);
    this.addRoomPopup = this.addRoomPopup.bind(this);
  }

  componentDidMount() {
    var that = this;
    auth.onAuthStateChanged(function (user) {
      if (user) {
        that.getFriends();
        var query = db.collection('users')
          .where('userName', '==', auth.currentUser.displayName);
        // Start listening to the query.
        query.onSnapshot(function (snapshot) {
          snapshot.docChanges().forEach(function (change) {
            var rooms = [];
            change.doc.data().rooms.forEach(room => {
              rooms.push(room)
            });
            that.setState({
              rooms: rooms
            });
          });
        });
      }
    });
  }

  onChangeHandler = event => {
    this.setState({ [event.target.name]: event.target.id });
  }

  onFriendChecked = event => {
    const { friendsInv } = this.state;
    if (event.target.checked) {
      friendsInv.push(event.target.id);
    } else {

      var elemIndex = friendsInv.findIndex(function (element) {
        return element === event.target.id;
      })

      friendsInv.splice(elemIndex, 1);
    }
  }

  onRoomInputChange = event => {
    this.setState({ roomInput: event.target.value });
  }

  getFriends() {
    this.setState({
      friends: []
    });

    var that = this;
    var query = db.collection('users')
      .where('userName', '==', auth.currentUser.displayName);

    // Start listening to the query.
    query.onSnapshot(function (snapshot) {
      snapshot.docChanges().forEach(function (change) {
        that.setState({
          friends: change.doc.data().friends
        });
      });
    });
  }

  addRoomPopup() {
    var that = this;
    const options = {
      customUI: ({ onClose }) => {
        function wrapper() {
          that.onSubmitRoom();
          onClose();
        }
        that.getFriends();
        var friendsToReact = this.state.friends.map(function (friend) {
          return <div>
            <input id={friend} type="checkbox" name='' onChange={that.onFriendChecked} />
            <label htmlFor={friend}>{friend}
            </label>
          </div>;
        })

        return (
          <div className="react-confirm-alert-body">
            <h1>Create a new room</h1>
            Enter the room name
                  <div>
              <input type="text" onChange={that.onRoomInputChange} />
            </div>
            <div>{friendsToReact}</div>
            <div className="react-confirm-alert-button-group">
              <button onClick={wrapper}>Create Room</button>
              <button onClick={onClose}>Cancel</button>
            </div>
          </div>)
      },
      closeOnEscape: true,
      closeOnClickOutside: true,
      willUnmount: () => { },
      onClickOutside: () => { },
      onKeypressEscape: () => { }
    };

    confirmAlert(options);
  }

  onSubmitRoom = event => {
    const { rooms, roomInput, friendsInv } = this.state;
    //event.preventDefault();
    if (roomInput === '') {
      alert("You have to fill in the room name");
      return;
    }

    if (rooms.includes(roomInput)) {
      alert("You're already in that room");
      return;
    }

    if (friendsInv.length === 0) {
      alert("You have to invite at least one friend");
      return;
    }

    rooms.push(roomInput);
    db.collection("users")
      .doc(auth.currentUser.email)
      .update({ rooms: rooms });

    friendsInv.forEach(friend => {

      var query = db
        .collection('users')
        .where('userName', '==', friend);

      query.onSnapshot(function (snapshot) {
        var rooms = snapshot.docs[0].data().rooms;

        if (!rooms.includes(roomInput)) {
          rooms.push(roomInput);
          db.collection("users")
            .doc(snapshot.docs[0].data().email)
            .update({ rooms: rooms });
        }
      });
    });
  }

  setHidden = (e) => {
    e.preventDefault();
    this.setState(prevState => ({
      hidden: !prevState.hidden
    }));
  }

  render() {
    var that = this;
    var roomsReact = this.state.rooms.map(function (room) {
      return <button id={room} key={room} className="nes-btn is-primary" name='currentRoom' onClick={that.onChangeHandler}>
        {room}
      </button>;
    })
    const { location } = this.props;
    if (location.pathname.match(/game/) || location.pathname.match(/login-register/)) {
      return null;
    } else {
      return (
        <UserSessionContext.Consumer>
          {context => (context.currentUser &&
            <div className="chat">
              <div className="chat-hide">
                <a href="/" onClick={this.setHidden}>Chat</a>
              </div>
              <div className="chat-content row" style={{ display: this.state.hidden ? "none" : "flex" }}>
                <div className="col-2 rooms-col">
                  <p>Rooms</p>
                  {roomsReact}
                  <button onClick={this.addRoomPopup} className="nes-btn is-error" >+</button>
                </div>
                <MessageList currentRoom={this.state.currentRoom} />
                <FriendList />
                <ChatUsers currentRoom={this.state.currentRoom} />
              </div>
            </div>)}
        </UserSessionContext.Consumer>
      );
    }
  }
}

export default withRouter(Chat);