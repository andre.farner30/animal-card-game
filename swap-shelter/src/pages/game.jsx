import React, { Component } from "react";

//Custome styles
import '../styles/game.css';

//Custom components
import SocketContext from '../components/game/SocketContext';
import JoinHost from "../components/game/JoinHost";
import Lobby from "../components/game//lobby/Lobby";
import InGame from "../components/game/inGame/InGame";

/*External libraries*/
//Socket io docs - https://socket.io/docs/
import io from "socket.io-client";

//React DnD: DnDProvider docs - https://react-dnd.github.io/react-dnd/docs/api/dnd-provider
import { DndProvider } from 'react-dnd';

//React DnD: HTML5 Backend docs - https://react-dnd.github.io/react-dnd/docs/backends/html5
import HTML5Backend from 'react-dnd-html5-backend';

//React DnD: Touch Backend docs - https://github.com/redonkulus/react-dnd-touch-backend
import TouchBackend from 'react-dnd-touch-backend';

import { UserSessionContext } from '../components/login-register/CurrentUserContext';

let socket;

class Game extends Component {
    constructor(props) {
        super(props);
        socket = io("https://andre-farner30-animal-card-game.34.77.78.243.nip.io/game");
        this.state = {
            game: "",
            lobby: false,
            inGame: false,
            player: undefined,
            opponent: undefined,
            rules: undefined,
            turnTime: undefined,
            gameInfo: undefined
        };
    }

    updateGameInfo = (newGameInfo) => {
        this.setState({ 
            gameInfo: newGameInfo,
         });
    }

    componentDidMount() {
        socket.on('lobbyleftset', (msg) => {
            if (msg) {
                this.setState({
                    lobby: true,
                    player: msg.player,
                    rules: msg.rules,
                    game: msg.room
                });
            }
        });

        socket.on('opponentsetup', (msg) => {
            this.setState({
                opponent: msg
            });
        });

        socket.on("rulechange", (msg) => {
            this.setState(prevState => ({
                rules: {
                    ...prevState.rules,
                    [msg.rule]: msg.checked
                }
            }));
        });

        socket.on("turntimechange", (msg) => {
            this.setState({
                turnTime: msg
            });
        });

        socket.on('gamestart', (msg) => {
            if (this.state.player.username === msg.game.player1.username) {
                this.setState({
                    player: msg.game.player1,
                    opponent: msg.game.player2
                },
                    () => {
                        this.setState({
                            gameInfo: msg.game,
                            lobby: false,
                            inGame: true,
                            turnTime: msg.turnTime
                        });
                    });
            } else {
                this.setState({
                    player: msg.game.player2,
                    opponent: msg.game.player1
                },
                    () => {
                        this.setState({
                            gameInfo: msg.game,
                            lobby: false,
                            inGame: true,
                            turnTime: msg.turnTime
                        });
                    });
            }
        });
    }

    componentWillUnmount() {
        this.setState({
            player: undefined
        });
        socket.disconnect();
    }

    //Check for mobile devices
    isMobile = () => {
        if ('ontouchstart' in document.documentElement) {
            return TouchBackend;
        } else {
            return HTML5Backend;
        }
    }

    render() {
        return (
            <SocketContext.Provider value={socket}>
                <div id="game">
                    {this.state.lobby ?
                        <Lobby lobbyInfo={{
                            player: this.state.player, opponent: this.state.opponent, rules: this.state.rules, room: this.state.game, turnTime: this.state.turnTime
                        }} /> :
                        (this.state.inGame ?
                            <DndProvider backend={this.isMobile()}>
                                <SocketContext.Consumer>
                                    {value => <InGame
                                        gameInfo={this.state.gameInfo}
                                        players={{ player: this.state.player, opponent: this.state.opponent }}
                                        turnTime={this.state.turnTime}
                                        socket={value}
                                        updateGameInfo={this.updateGameInfo}
                                    />}
                                </SocketContext.Consumer>
                            </DndProvider> :
                            <SocketContext.Consumer>
                                {value =>
                                    <UserSessionContext.Consumer>
                                        {context => (context.currentUser &&
                                            <JoinHost socket={value} user={context.currentUser} />
                                        )}
                                    </UserSessionContext.Consumer>
                                }
                            </SocketContext.Consumer>
                        )}
                </div>
            </SocketContext.Provider>
        );
    }
}

export default Game;