import React, { Component } from "react";
import '../styles/style.css';

import Header from "../components/Header";
import MainBody from "../components/mainPage/MainBody";
import Footer from "../components/footer/Footer";

class MainPage extends Component {
    render() {
        return (
            <div>
                <Header />
                <MainBody />
                <Footer />
            </div>
        );
    }
}

MainPage.defaultProps = {
    currentUser: null
}

export default MainPage;