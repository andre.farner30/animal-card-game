import React from "react";
import '../styles/l2play.css';

import Header from "../components/Header";
import Footer from "../components/footer/Footer";

const L2Play = () => {
    return (
        <div>
            <Header />
            <div className="main">
                <div className="row main-section-header my-sm-4 mt-sm-5">
                    <div className="col">
                        <h1 className="nes-text is-error mb-sm-5">LEARN 2 PLAY</h1>
                        <div className="col nes-container is-rounded with-title text-justify">
                            <p className="title">Introduction</p>
                            <p>Here you'll find a brief explanation of everything you need to know to demolish the
                            competition!</p>
                            <p>Bellow follows a series of explanations of each game modifier that will give you a better
                            ideia of how the game works.</p>
                        </div>
                    </div>
                </div>
                <div className="row main-section-content">
                    <div className="col learn-game-rules">
                        <div className="col nes-container is-rounded text-justify" id="learn-game-rules-passive">
                            <p><b>First we'll talk about the passive rules.</b></p>
                            <p>Passive rules are rules that only apply at the beginning or the end of each match.</p>
                            <p>They affect the overall dynamic of the match, giving it a different feel to a regular game.</p>
                        </div>
                        <section className="message-list col">
                            <section className="row message -left my-sm-4 mb-sm-5" id="learn-game-rules-open">
                                <div className="col-1 rule-icon-left">
                                    <i className="fas fa-dog fa-5x"></i>
                                </div>
                                <div className="col-11">
                                    <div className="row nes-balloon from-left message-inner">
                                        <div className="col-3 rule-preview" id="learn-game-rules-open-gif">
                                            <img src="images/preview.gif" alt="rule1" />
                                        </div>
                                        <div className="col-9" id="learn-game-rules-open-expl">
                                            <p className="text-justify text-left"><h4 className="nes-text is-error">(Open Rule)</h4> The Open Rule makes it so that both players can see the opponents animals'
                                                values. Completely removing any ambiguity of which animals the enemy holds this match,
                                                making it so that you're required to look farther ahead than you're enemy to claim victory!</p>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section className="row message -right my-sm-4 mb-sm-5" id="learn-game-rules-suddendeath">
                                <div className="col-11">
                                    <div className="row nes-balloon from-right message-inner">
                                        <div className="col-9" id="learn-game-rules-suddendeath-expl">
                                            <p className="text-justify text-left"><h4 className="nes-text is-error">(Sudden Death Rule)</h4> The Sudden Death Rule gets rid of tied games. If you finish a game
                                            while tied and happen to have this rule on, you'll immediately start another match. The
                                            catch being that the new match you'll play with whichever cards you had captured the
                                            previous round. This will repeat ad nauseum until there's an ultimate victor!</p>
                                        </div>
                                        <div className="col-3 rule-preview" id="learn-game-rules-suddendeath-gif">
                                            <img src="images/preview.gif" alt="rule1" />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-1 rule-icon-right">
                                    <i className="fas fa-cat fa-5x"></i>
                                </div>
                            </section>
                        </section>
                        <hr id="mid-rules-line" />
                        <div className="col nes-container is-rounded text-justify" id="learn-game-rules-active">
                            <p className="text-justify text-left"><b>In this section we will consider the active rules.</b></p>
                            <p className="text-justify text-left">Active rules are rules that activate as soon as each player makes their move.</p>
                            <p className="text-justify text-left">These significantly affect the way each play happens, leading to a severe change in the way
                                the game is played in more minute ways. While these lack the overall impact the passive
                                rules hold, if one or more of these modifiers are in play the chances of sudden comeback are
                                higher and higher!
                                *Explanatory image/gif here*</p>
                        </div>
                        <section class="message-list col">
                            <section className="row message -left my-sm-4 mb-sm-5" id="learn-game-rules-same">
                                <div className="col-1 rule-icon-left">
                                    <i className="fas fa-crow fa-5x"></i>
                                </div>
                                <div className="col-11">
                                    <div className="row nes-balloon from-left message-inner">
                                        <div className="col-3 rule-preview" id="learn-game-rules-same-gif">
                                            <img src="images/preview.gif" alt="rule1" />
                                        </div>
                                        <div className="col-9" id="learn-game-rules-same-expl">
                                            <p className="text-justify text-left"><h4 className="nes-text is-error">(Same Rule)</h4> The Same Rule is quite simple, if your animal is in contact with 2 or more
                                            adversarial animals and the values on your card match the values on the enemies cards'
                                            sides, you will capture all of the opponents' animals where this applies.*Multiple
                                            explanatory images (3+)/gif here*</p>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section className="row message -right my-sm-4 mb-sm-5" id="learn-game-rules-plus">
                                <div className="col-11">
                                    <div className="row nes-balloon from-right message-inner">
                                        <div className="col-9" id="learn-game-rules-plus-expl">
                                            <p className="text-justify text-left"><h4 className="nes-text is-error">(Plus Rule)</h4> The Plus Rule is quite similar to the Same Rule, except when this modifier is
                                            active and your animal is in contact with 2 or more of the opponents animals, you will
                                            capture those animals only if the sum of the adjacent sides of your animal and theirs
                                            match.*Multiple explanatory images (3+)/gif here*</p>
                                        </div>
                                        <div className="col-3 rule-preview" id="learn-game-rules-plus-gif">
                                            <img src="images/preview.gif" alt="rule1" />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-1 rule-icon-right">
                                    <i className="fas fa-frog fa-5x"></i>
                                </div>
                            </section>
                            <section className="row message -left my-sm-4 mb-sm-5" id="learn-game-rules-combo">
                                <div className="col-1 rule-icon-left">
                                    <i className="fas fa-dog fa-5x"></i>
                                </div>
                                <div className="col-11">
                                    <div className="row nes-balloon from-left message-inner">
                                        <div className="col-3 rule-preview" id="learn-game-rules-combo-gif">
                                            <img src="images/preview.gif" alt="rule1" />
                                        </div>
                                        <div className="col-9" id="learn-game-rules-combo-expl">
                                            <p className="text-justify text-left"><h4 className="nes-text is-error">(Combo Rule)</h4> The Combo Rule will only apply in consequence of a play that triggered either the Plus or
                                            Same Rule so if either of those aren't in play, this modifier will be inaccessible. When
                                            one of the previous 2 rules active, the Combo Rule makes it so that the newly captured
                                            animals are recompared with the adversaries' animals that they are in contact with,
                                            triggering a capture only if the values of the sides that are in contact are higher in
                                            your new animals. This rule is a proponent of the biggest comeback plays in the game,
                                            because any cards captured by the Combo Rule are again subjected to the same rule,
                                            permiting overwhelming combos that capture every enemy animal.*Multiple explanatory
                                            images (3+)/gif here*</p>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section className="row message -right my-sm-4 mb-sm-5" id="learn-game-rules-elemental">
                                <div className="col-11">
                                    <div className="row nes-balloon from-right message-inner">
                                        <div className="col-9" id="learn-game-rules-elemental-expl">
                                            <p className="text-justify text-left"><h4 className="nes-text is-error">(Elemental Rule)</h4> The Elemental Rule modifies the playing field itself, assigning an element to some board
                                            slots randomly. Some cards also have elemental values and if that cards element matches
                                            the slots element, that card receives a +1 buff to every one of its values, whereas if
                                            the card element doesn't match the slots, that card receives a -1 one debuff. If it so
                                            happens that a specific board slot doesn't have an element, neither a buff or a debuff
                                            will be applied to the card.*Multiple explanatory images (2+)/gif here*</p>
                                        </div>
                                        <div className="col-3 rule-preview" id="learn-game-rules-elemental-gif">
                                            <img src="images/preview.gif" alt="rule1" />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-1 rule-icon-right">
                                    <i className="fas fa-cat fa-5x"></i>
                                </div>
                            </section>
                        </section>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default L2Play;