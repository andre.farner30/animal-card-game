import React from "react";

//Custom components
import Header from "../components/Header";
import TreatList from "../components/purchaseTreats/TreatList";

function PurchaseTreats() {
    return (
        <div>
            <Header />
            <div className="main">
                <h1 className="nes-text is-error my-sm-5">Buy Treats!</h1>
                <div className="row text-justify my-sm-5 nes-container is-rounded with-title">
                    <p className="title">Description</p>
                    <p><b className="nes-text is-error">Treats</b> are the currency used on this platform! It's a way for us to
                    make it so you have an easier time buying new, fun and exciting cosmetics or new cards without the hassle of going
                    through a complicated process every time you want to buy something!</p>
                    <p>Just buy as many <b className="nes-text is-error">Treats</b> as you want here, then browse our shop and easily spend them!</p>
                </div>
                <TreatList />
            </div>
        </div>
    );
}

export default PurchaseTreats;