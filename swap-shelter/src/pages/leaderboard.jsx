import React from "react";

//Custom components
import Header from "../components/Header";
import LeaderboardTable from "../components/leaderboard/LeaderboardTable";

function Leaderboard() {
    return (
        <div>
            <Header />
            <div className="main">
                <h1 className="nes-text is-error my-sm-5">LEADERBOARD</h1>
                <LeaderboardTable />
            </div>
        </div>
    );
}

export default Leaderboard;