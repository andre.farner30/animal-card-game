import React from "react";

//Custom components
import Header from "../components/Header";
import ShopContent from "../components/shop/ShopContent";
import { UserSessionContext } from '../components/login-register/CurrentUserContext';

function Shop() {
    return (
        <div>
            <Header />
            <div className="main">
                <UserSessionContext.Consumer>
                    {context => (context.currentUser &&
                        <ShopContent email={context.currentUser.email} updateUser={context.updateUser} />
                    )}
                </UserSessionContext.Consumer>
            </div>
        </div>
    );
}

export default Shop;