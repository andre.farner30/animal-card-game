import React from "react";

//Custom components
import Header from "../components/Header";
import ProfileInfo from "../components/profile/ProfileInfo";
import ProfileContent from "../components/profile/ProfileContent";

import { UserSessionContext } from '../components/login-register/CurrentUserContext';

function Profile() {
    return (
        <div>
            <Header />
            <UserSessionContext.Consumer>
                {context => (context.currentUser ?
                    <div className="main">
                        <ProfileInfo user={context.currentUser} />
                        <ProfileContent user={context.currentUser} />
                    </div> : null)}
            </UserSessionContext.Consumer>
        </div>
    );
}

export default Profile;