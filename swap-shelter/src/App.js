import React, { Component } from 'react';

import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";

import CurrentUserContext from './components/login-register/CurrentUserContext';
import { UserSessionContext } from './components/login-register/CurrentUserContext';

//pages
import MainPage from "./pages";
import Game from "./pages/game";
import LoginRegister from "./pages/login-register";
import L2Play from "./pages/l2play";
import Leaderboard from "./pages/leaderboard";
import Profile from "./pages/profile";
import PurchaseTreats from "./pages/purchase-treats";
import Shop from "./pages/shop";
import Chat from "./pages/chat";
import NotFound from "./pages/404";

class App extends Component {
  render() {
    return (
      <CurrentUserContext >
        <Router>
          <Switch>
            <Route exact path="/" component={MainPage} />
            <ProtectedRoute exact path="/game" component={Game} />
            <Route exact path="/l2play" component={L2Play} />
            <Route exact path="/leaderboard" component={Leaderboard} />
            <ProtectedRoute exact path="/profile" component={Profile} />
            <ProtectedRoute exact path="/purchase-treats" component={PurchaseTreats} />
            <ProtectedRoute exact path="/shop" component={Shop} />
            <Route exact path="/login-register" component={LoginRegister} />
            <Route exact path="/404" component={NotFound} />
            <Redirect to="/404" />
          </Switch>
          <Chat />
        </Router>
        <pre id="copyright">  &copy; 2019 - Shelter Swap</pre>
      </CurrentUserContext>
    );
  }
}
class ProtectedRoute extends Component {
  render() {
    const { component: Component, ...props } = this.props

    return (
      <Route
        {...props}
        render={props => (
          <UserSessionContext.Consumer>
            {context => (context.currentUser ?
              <Component {...props} /> :
              <Redirect to='/login-register' />
            )}
          </UserSessionContext.Consumer>
        )}
      />
    )
  }
}


export default App;
