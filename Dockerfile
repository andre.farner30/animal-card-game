FROM node:carbon

# Define environment
ENV NODE_ENV production

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Bundle app source
COPY . /usr/src/app

# Install server dependencies
RUN npm install

# Install react dependencies
RUN cd swap-shelter && npm install

# Build React - It returns to app directory automatically, no need to cd ..
RUN cd swap-shelter && npm run build

# Expose container port
EXPOSE 5000

CMD npm start
