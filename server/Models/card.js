class Card {
    constructor(id, name, left, right, up, down, elemental, rank, img) {
        this.id = id;
        this.name = name;
        this.left = left;
        this.right = right;
        this.up = up;
        this.down = down;
        this.elemental = elemental;
        this.rank = rank;
        this.img = img;
    }
}

Card.prototype.addValue = function () {
    this.left++;
    this.right++;
    this.up++;
    this.down++;

    if (this.left > 10) {
        this.left = 10;
    }
    if (this.right > 10) {
        this.right = 10;
    }
    if (this.up > 10) {
        this.up = 10;
    }
    if (this.down > 10) {
        this.down = 10;
    }
};

Card.prototype.subtractValue = function () {
    this.left--;
    this.right--;
    this.up--;
    this.down--;

    if (this.left < 1) {
        this.left = 1;
    }
    if (this.right < 1) {
        this.right = 1;
    }
    if (this.up < 1) {
        this.up = 1;
    }
    if (this.down < 1) {
        this.down = 1;
    }
};

module.exports = Card;