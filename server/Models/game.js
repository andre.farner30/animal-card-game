var Card = require("./card");
var Player = require("./player");

//  "Water", "Fire", "Earth", "Air"
var elementals = ["cat", "dog"];

class GameCell {
    constructor(card, hasElem) {
        this.card = card;
        if (hasElem) {
            if ((Math.floor(Math.random() * 11)) < 3) {
                this.elem = elementals[Math.floor(Math.random() * 2)];
            } else {
                this.elem = "X"
            }
        }
        this.color = undefined;
    }
}

class Game {
    constructor(gameName, player1, player2, options) {
        this.gameName = gameName;
        this.player1 = player1;
        this.player2 = player2;
        this.player1.score = this.player2.score = 5;
        this.isPlayer1 = true;
        this.inSuddenDeath = false;
        this.board = Array.from({ length: 3 }, (_) => Array.from({ length: 3 }, (_) => new GameCell(undefined, options.elemental)));
        this.options = options;
        this.lastPlay = {
            firstCard: undefined,
            captured: [],
            same: [],
            plus: [],
            combo: []
        };
    }
}

/**
 * 
 * @param {Card} newCard - The new card that was placed on the board
 * @param {int} xPosition - The row index of the position that was placed.
 * @param {int} yPosition - The collum index of the position that was placed.
 * 
 */
Game.prototype.update = function (newCard, xPosition, yPosition, isCombo, comboNumber = 0) {

    var playerColor = this.isPlayer1 ? "blue" : "red";
    var comboOrder = comboNumber;

    // Vizinhos filtrados pela cor aposta (Vizinhos do oponente)
    var neighbours = cellNeighbours(this.board, xPosition, yPosition).filter(n => this.board[n.x][n.y].color !== playerColor && this.board[n.x][n.y].card !== undefined);
    console.log(cellNeighbours(this.board, xPosition, yPosition));

    var same = {
        left: false,
        right: false,
        up: false,
        down: false
    };

    var plus = {
        left: 0,
        right: 0,
        up: 0,
        down: 0
    }

    neighbours.forEach(element => {
        var card = this.board[element.x][element.y].card;
        var hasRule = false;
        if (card) {
            //Check which neighbour
            var x = element.x - xPosition
            var y = element.y - yPosition

            //On the left
            if (y === -1) {
                //Para a verificação do same 
                if (card.right == newCard.left) {
                    hasRule = true;
                    same.left = true;
                }

                if (card.right < newCard.left) {
                    this.board[element.x][element.y].color = playerColor;

                    if (this.options.combo) {
                        if (isCombo) {
                            this.lastPlay.combo.push({ combo: comboOrder, card: card.name, pos: { x: element.x, y: element.y } });
                            this.update(this.board[element.x][element.y].card, element.x, element.y, true, comboOrder + 1);
                        } else {
                            this.lastPlay.captured.push({ x: element.x, y: element.y });
                            this.update(this.board[element.x][element.y].card, element.x, element.y, true, comboOrder);
                        }
                    } else {
                        this.lastPlay.captured.push({ x: element.x, y: element.y });
                    }

                } else {
                    //Para a verificação do plus
                    plus.left = card.right + newCard.left;
                }
            }

            //On the right
            if (y === 1) {
                //Para a verificação do same                
                if (card.left == newCard.right) {
                    same.right = true;
                }

                if (card.left < newCard.right) {
                    this.board[element.x][element.y].color = playerColor;

                    if (this.options.combo) {
                        if (isCombo) {
                            this.lastPlay.combo.push({ combo: comboOrder, card: card.name, pos: { x: element.x, y: element.y } });
                            this.update(this.board[element.x][element.y].card, element.x, element.y, true, comboOrder + 1);
                        } else {
                            this.lastPlay.captured.push({ x: element.x, y: element.y });
                            this.update(this.board[element.x][element.y].card, element.x, element.y, true, comboOrder);
                        }
                    } else {
                        this.lastPlay.captured.push({ x: element.x, y: element.y });
                    }

                } else {
                    //Para a verificação do plus
                    plus.right = card.left + newCard.right;
                }
            }

            //On the top
            if (x === -1) {
                //Para a verificação do same  
                if (card.down == newCard.up) {
                    same.up = true;
                }

                if (card.down < newCard.up) {
                    this.board[element.x][element.y].color = playerColor;

                    if (this.options.combo) {
                        if (isCombo) {
                            this.lastPlay.combo.push({ combo: comboOrder, card: card.name, pos: { x: element.x, y: element.y } });
                            this.update(this.board[element.x][element.y].card, element.x, element.y, true, comboOrder + 1);
                        } else {
                            this.lastPlay.captured.push({ x: element.x, y: element.y });
                            this.update(this.board[element.x][element.y].card, element.x, element.y, true, comboOrder);
                        }
                    } else {
                        this.lastPlay.captured.push({ x: element.x, y: element.y });
                    }

                } else {
                    //Para a verificação do plus
                    plus.up = card.down + newCard.up;
                }
            }

            //On the bottom
            if (x === 1) {
                //Para a verificação do same 
                if (card.up == newCard.down) {
                    same.down = true;
                }

                if (card.up < newCard.down) {
                    this.board[element.x][element.y].color = playerColor;

                    if (this.options.combo) {
                        if (isCombo) {
                            this.lastPlay.combo.push({ combo: comboOrder, card: card.name, pos: { x: element.x, y: element.y } });
                            this.update(this.board[element.x][element.y].card, element.x, element.y, true, comboOrder + 1);
                        } else {
                            this.lastPlay.captured.push({ x: element.x, y: element.y });
                            this.update(this.board[element.x][element.y].card, element.x, element.y, true, comboOrder);
                        }
                    } else {
                        this.lastPlay.captured.push({ x: element.x, y: element.y });
                    }

                } else {
                    //Para a verificação do plus 
                    plus.down = card.up + newCard.down;
                }
            }
        }
    });

    var hasSame = false;
    //Prioridade
    if (this.options.same && !isCombo) {

        var sameNeighbours = Object.keys(same).filter(x => same[x]).map(String);

        if (sameNeighbours.length > 1) {
            hasSame = true;
            sameNeighbours.forEach(element => {
                switch (element) {
                    case "left":
                        this.board[xPosition][yPosition - 1].color = playerColor;
                        this.lastPlay.same.push({ x: xPosition, y: yPosition - 1 });
                        if (this.options.combo) {
                            this.update(this.board[xPosition][yPosition - 1].card, xPosition, yPosition - 1, true, comboOrder);
                        }
                        break;
                    case "right":
                        this.board[xPosition][yPosition + 1].color = playerColor;
                        this.lastPlay.same.push({ x: xPosition, y: yPosition + 1 });
                        if (this.options.combo) {
                            this.update(this.board[xPosition][yPosition + 1].card, xPosition, yPosition + 1, true, comboOrder);
                        }
                        break;
                    case "up":
                        this.board[xPosition - 1][yPosition].color = playerColor;
                        this.lastPlay.same.push({ x: xPosition - 1, y: yPosition });
                        if (this.options.combo) {
                            this.update(this.board[xPosition - 1][yPosition].card, xPosition - 1, yPosition, true, comboOrder);
                        }
                        break;
                    case "down":
                        this.board[xPosition + 1][yPosition].color = playerColor;
                        this.lastPlay.same.push({ x: xPosition + 1, y: yPosition });
                        if (this.options.combo) {
                            this.update(this.board[xPosition + 1][yPosition].card, xPosition + 1, yPosition, true, comboOrder);
                        }
                        break;
                    default:
                        break;
                }
            });
        }
    }

    //After Same
    if (this.options.plus && !isCombo && !hasSame) {
        var plusNeighbours = Object.keys(plus).filter(x => plus[x] !== 0).map(String);

        if (plusNeighbours.length > 1) {
            plusNeighbours.forEach(element => {

                if (element === "left") {
                    switch (plus[element]) {
                        case plus.right:
                            this.board[xPosition][yPosition + 1].color = playerColor;
                            this.lastPlay.plus.push({ x: xPosition, y: yPosition + 1 });
                            if (this.options.combo) {
                                this.update(this.board[xPosition][yPosition + 1].card, xPosition, yPosition + 1, true, comboOrder);
                            }
                            break;
                        case plus.up:
                            this.board[xPosition - 1][yPosition].color = playerColor;
                            this.lastPlay.plus.push({ x: xPosition - 1, y: yPosition });
                            if (this.options.combo) {
                                this.update(this.board[xPosition - 1][yPosition].card, xPosition - 1, yPosition, true, comboOrder);
                            }
                            break;
                        case plus.down:
                            this.board[xPosition + 1][yPosition].color = playerColor;
                            this.lastPlay.plus.push({ x: xPosition + 1, y: yPosition });
                            if (this.options.combo) {
                                this.update(this.board[xPosition + 1][yPosition].card, xPosition + 1, yPosition, true, comboOrder);
                            }
                            break;
                        default:
                            break;
                    }
                }

                if (element == "right") {
                    switch (plus[element]) {
                        case plus.left:
                            this.board[xPosition][yPosition - 1].color = playerColor;
                            this.lastPlay.plus.push({ x: xPosition, y: yPosition - 1 });
                            if (this.options.combo) {
                                this.update(this.board[xPosition][yPosition - 1].card, xPosition, yPosition - 1, true, comboOrder);
                            }
                            break;
                        case plus.up:
                            this.board[xPosition - 1][yPosition].color = playerColor;
                            this.lastPlay.plus.push({ x: xPosition - 1, y: yPosition });
                            if (this.options.combo) {
                                this.update(this.board[xPosition - 1][yPosition].card, xPosition - 1, yPosition, true, comboOrder);
                            }
                            break;
                        case plus.down:
                            this.board[xPosition + 1][yPosition].color = playerColor;
                            this.lastPlay.plus.push({ x: xPosition + 1, y: yPosition });
                            if (this.options.combo) {
                                this.update(this.board[xPosition + 1][yPosition].card, xPosition + 1, yPosition, true, comboOrder);
                            }
                            break;
                        default:
                            break;
                    }
                }

                if (element == "up") {
                    switch (plus[element]) {
                        case plus.left:
                            this.board[xPosition][yPosition - 1].color = playerColor;
                            this.lastPlay.plus.push({ x: xPosition, y: yPosition - 1 });
                            if (this.options.combo) {
                                this.update(this.board[xPosition][yPosition - 1].card, xPosition, yPosition - 1, true, comboOrder);
                            }
                            break;
                        case plus.right:
                            this.board[xPosition][yPosition + 1].color = playerColor;
                            this.lastPlay.plus.push({ x: xPosition, y: yPosition + 1 });
                            if (this.options.combo) {
                                this.update(this.board[xPosition][yPosition + 1].card, xPosition, yPosition + 1, true, comboOrder);
                            }
                            break;
                        case plus.down:
                            this.board[xPosition + 1][yPosition].color = playerColor;
                            this.lastPlay.plus.push({ x: xPosition + 1, y: yPosition });
                            if (this.options.combo) {
                                this.update(this.board[xPosition + 1][yPosition].card, xPosition + 1, yPosition, true, comboOrder);
                            }
                            break;
                        default:
                            break;
                    }
                }

                if (element == "down") {
                    switch (plus[element]) {
                        case plus.left:
                            this.board[xPosition][yPosition - 1].color = playerColor;
                            this.lastPlay.plus.push({ x: xPosition, y: yPosition - 1 });
                            if (this.options.combo) {
                                this.update(this.board[xPosition][yPosition - 1].card, xPosition, yPosition - 1, true, comboOrder);
                            }
                            break;
                        case plus.right:
                            this.board[xPosition][yPosition + 1].color = playerColor;
                            this.lastPlay.plus.push({ x: xPosition, y: yPosition + 1 });
                            if (this.options.combo) {
                                this.update(this.board[xPosition][yPosition + 1].card, xPosition, yPosition + 1, true, comboOrder);
                            }
                            break;
                        case plus.up:
                            this.board[xPosition - 1][yPosition].color = playerColor;
                            this.lastPlay.plus.push({ x: xPosition - 1, y: yPosition });
                            if (this.options.combo) {
                                this.update(this.board[xPosition - 1][yPosition].card, xPosition - 1, yPosition, true, comboOrder);
                            }
                            break;
                        default:
                            break;
                    }
                }
            });
        }
    }
};

/**
 * @param {int} pickedCardIndex - The index if the pickedCard.
 * @param {int} xPosition - The row index of the position picked.
 * @param {int} yPosition - The collum index of the position picked.
 * @description returns the correspondent value of the play [ 1 - Sucessfull, 2 - Position already filled, 3 - End Game, 4 - No cards at index picked ]
 */
Game.prototype.play = function (pickedCardIndex, xPosition, yPosition) {

    this.lastPlay.captured = [];
    this.lastPlay.same = [];
    this.lastPlay.plus = [];
    this.lastPlay.combo = [];
    var currPlayer = this.isPlayer1 ? this.player1 : this.player2;
    var playerColor = this.isPlayer1 ? "blue" : "red";

    if (currPlayer.hand.length === 0 || currPlayer.hand[pickedCardIndex] === undefined) {
        console.log("No more cards at hand or no card at cardIndex");
        return 4;
    }

    if (this.board[xPosition][yPosition].card !== undefined) {
        console.log("Position already filled");
        return 2;
    }

    this.board[xPosition][yPosition].card = currPlayer.hand[pickedCardIndex];
    this.board[xPosition][yPosition].color = playerColor;

    if (this.options.elemental && this.board[xPosition][yPosition].elem !== "X") {
        if (this.board[xPosition][yPosition].card.elemental === this.board[xPosition][yPosition].elem) {
            this.board[xPosition][yPosition].card.addValue();
        } else {
            this.board[xPosition][yPosition].card.subtractValue();
        }
    }

    this.lastPlay.firstCard = { x: xPosition, y: yPosition, card: currPlayer.hand[pickedCardIndex] };
    this.update(currPlayer.hand[pickedCardIndex], xPosition, yPosition, false, 1);

    currPlayer.hand[pickedCardIndex] = undefined;
    this.setScores();
    if (this.isMatrixFull()) {
        // if (options.suddenDeath && this.player1.score === this.player2.score) {
        //     this.suddenDeath();
        // } else {
        console.log("Game ended");
        return 3;
        // }
    }

    this.isPlayer1 = !this.isPlayer1;
    return 1;
};

Game.prototype.printBoard = function () {
    var print = "";

    console.log("\n");
    this.board.forEach(row => {
        row.forEach(cell => {
            if (cell.card) {
                print += cell.card.name + "L:" + cell.card.left + " R:" + cell.card.right + " U:" + cell.card.up + " D:" + cell.card.down + " " + cell.elem + " " + cell.color + " | ";
            } else {
                print += "Empty ################## |";
            }
        });
        print += "\n";
    });

    console.log(print);
};

Game.prototype.setScores = function () {
    this.player1.score = this.player1.hand.filter(card => card).length;
    this.player2.score = this.player2.hand.filter(card => card).length;
    this.board.forEach(i => {
        i.forEach(j => {
            if (j.color === 'blue') {
                this.player1.score++;
            }
            if (j.color === 'red') {
                this.player2.score++;
            }
        });
    });
    console.log('SCORES:');
    console.log(this.player1.score);
    console.log(this.player2.score);
};

Game.prototype.isMatrixFull = function () {
    var isFull = true;
    this.board.forEach(i => {
        i.forEach(j => {
            if (j.color === undefined) {
                isFull = false;
            }
        });
    });
    return isFull;
};

Game.prototype.suddenDeath = function () {
    var player1Cards = [];
    var player2Cards = [];
    this.board.forEach(i => {
        i.forEach(j => {
            if (j.color === 'red') {
                player1Cards.push(j.card);
            } else {
                player2Cards.push(j.card);
            }
        });
    });
    this.board = Array.from({ length: 3 }, (_) => Array.from({ length: 3 }, (_) => new GameCell(undefined, options.elemental)));
};

/**
 * 
 * @param {array} matrix - The matrix to check neighbours
 * @param {int} xPosition - The row index of the cell 
 * @param {int} yPosition - The collum index of the cell
 * 
 */
function cellNeighbours(matrix, xPosition, yPosition) {
    var neighbours = [];

    if (matrix[xPosition]) {
        if (matrix[xPosition][yPosition + 1]) {
            neighbours.push({
                x: xPosition,
                y: yPosition + 1
            });
        }

        if (matrix[xPosition][yPosition - 1]) {
            neighbours.push({
                x: xPosition,
                y: yPosition - 1
            });
        }
    }

    if (matrix[xPosition + 1]) {
        if (matrix[xPosition + 1][yPosition]) {
            neighbours.push({
                x: xPosition + 1,
                y: yPosition
            });
        }
    }

    if (matrix[xPosition - 1]) {
        if (matrix[xPosition - 1][yPosition]) {
            neighbours.push({
                x: xPosition - 1,
                y: yPosition
            });
        }
    }
    return neighbours;
}


//#################################    Testing 101    ###############################################

//##### Standard situation  ############

function standExample() {

    var testHand1 = [
        new Card("p1c1", 1, 2, 4, 1),
        new Card("p1c2", 1, 1, 1, 1),
        new Card("p1c3", 1, 1, 1, 2),
        new Card("p1c4", 1, 3, 0, 5),
        new Card("p1c5", 1, 4, 1, 3)
    ];

    var testHand2 = [
        new Card("p2c1", 1, 1, 4, 6),
        new Card("p2c2", 1, 1, 2, 7),
        new Card("p2c3", 1, 3, 1, 3),
        new Card("p2c4", 2, 2, 0, 2),
        new Card("p2c5", 1, 4, 1, 3)
    ];

    var testPlayer1 = new Player('Tester1', testHand1);
    var testPlayer2 = new Player('Tester2', testHand2);

    var testGame = new Game("memes", testPlayer1, testPlayer2, options);

    testGame.play(0, 2, 2);
    testGame.play(0, 0, 1);
    testGame.play(1, 0, 2);
    testGame.play(1, 1, 0);
    testGame.play(2, 1, 2);
    testGame.play(2, 1, 1);

    testGame.play(3, 2, 0);
    testGame.play(3, 2, 1);

    console.log("##### LAST PLAYED #####\n");

    testGame.play(4, 0, 0);


    console.log(testGame.lastPlay);
    testGame.printBoard();

}


//#### SAME situation  ##########

function sameExample() {

    var testHand1 = [
        new Card("p1c1", 1, 2, 1, 1),
        new Card("p1c2", 1, 1, 1, 1),
        new Card("p1c3", 1, 1, 1, 2),
        new Card("p1c4", 1, 1, 0, 2),
        new Card("p1c5", 1, 4, 1, 3)
    ];

    var testHand2 = [
        new Card("p2c1", 1, 1, 4, 6),
        new Card("p2c2", 1, 1, 2, 1),
        new Card("p2c3", 1, 1, 1, 1),
        new Card("p2c4", 2, 2, 0, 2),
        new Card("p2c5", 1, 4, 1, 3)
    ];

    var testPlayer1 = new Player('Tester1', testHand1);
    var testPlayer2 = new Player('Tester2', testHand2);

    var testGame = new Game(testPlayer1, testPlayer2, options);

    testGame.play(0, 2, 2);
    testGame.play(0, 0, 1);
    testGame.play(0, 0, 2);
    testGame.play(0, 1, 0);
    testGame.play(0, 1, 2);
    testGame.play(0, 1, 1);
    testGame.play(0, 0, 0);

    console.log(testGame.lastPlay);
    testGame.printBoard();
}

//########  PLUS situation  ###########

function plusExample() {
    var testHand1 = [
        new Card("p1c1", 1, 2, 1, 1),
        new Card("p1c2", 1, 1, 1, 1),
        new Card("p1c3", 1, 1, 1, 2),
        new Card("p1c4", 1, 2, 0, 3),
        new Card("p1c5", 1, 4, 1, 3)
    ];

    var testHand2 = [
        new Card("p2c1", 5, 1, 4, 6),
        new Card("p2c2", 1, 1, 4, 1),
        new Card("p2c3", 1, 1, 1, 1),
        new Card("p2c4", 2, 2, 0, 2),
        new Card("p2c5", 1, 4, 1, 3)
    ];

    var testPlayer1 = new Player('Tester1', testHand1);
    var testPlayer2 = new Player('Tester2', testHand2);

    var testGame = new Game(testPlayer1, testPlayer2, options);

    testGame.play(0, 2, 2);
    testGame.play(0, 0, 1);
    testGame.play(0, 0, 2);
    testGame.play(0, 1, 0);
    testGame.play(0, 1, 2);
    testGame.play(0, 1, 1);
    testGame.play(0, 0, 0);

    console.log(testGame.lastPlay);
    testGame.printBoard();
}

//########  ELEM situation  ###########

function elemExample() {
    var testHand1 = [
        new Card("p1c1", 1, 1, 1, 1, "F"),
        new Card("p1c2", 1, 1, 1, 1, "F"),
        new Card("p1c3", 1, 1, 1, 1, "F"),
        new Card("p1c4", 1, 1, 1, 1, "F"),
        new Card("p1c5", 1, 1, 1, 1, "F")
    ];

    var testHand2 = [
        new Card("p2c1", 1, 1, 1, 1, "W"),
        new Card("p2c2", 1, 1, 1, 1, "W"),
        new Card("p2c3", 1, 1, 1, 1, "W"),
        new Card("p2c4", 1, 1, 1, 1, "W"),
        new Card("p2c5", 1, 1, 1, 1, "W")
    ];

    var testPlayer1 = new Player('Tester1', testHand1);
    var testPlayer2 = new Player('Tester2', testHand2);

    var testGame = new Game(testPlayer1, testPlayer2, options);

    testGame.play(0, 2, 2);
    testGame.play(0, 0, 1);
    testGame.play(0, 0, 2);
    testGame.play(0, 1, 0);
    testGame.play(0, 1, 2);
    testGame.play(0, 1, 1);
    testGame.play(0, 0, 0);

    console.log(testGame.lastPlay);
    testGame.printBoard();
}

//########  SUDDEN situation  ###########

function suddenDeathExample() {

    var testHand1 = [
        new Card("p1c1", 3, 2, 4, 1),
        new Card("p1c2", 1, 1, 1, 1),
        new Card("p1c3", 4, 1, 1, 2),
        new Card("p1c4", 1, 3, 0, 5),
        new Card("p1c5", 1, 1, 1, 3)
    ];

    var testHand2 = [
        new Card("p2c1", 1, 1, 4, 6),
        new Card("p2c2", 1, 1, 6, 7),
        new Card("p2c3", 1, 3, 1, 3),
        new Card("p2c4", 0, 2, 0, 2),
        new Card("p2c5", 1, 4, 1, 3)
    ];

    var testPlayer1 = new Player('Tester1', testHand1);
    var testPlayer2 = new Player('Tester2', testHand2);

    var testGame = new Game(testPlayer1, testPlayer2, options);

    testGame.play(0, 2, 2);
    testGame.play(0, 0, 1);
    testGame.play(0, 0, 2);
    testGame.play(0, 1, 0);
    testGame.play(0, 1, 2);
    testGame.play(0, 1, 1);
    testGame.play(0, 2, 0);
    testGame.play(0, 2, 1);
    testGame.play(0, 0, 0);

    testGame.printBoard();
}

var options = {
    open: true,          //-----
    same: true,          //Feito
    plus: true,          //Feito
    combo: true,          //Feito
    elemental: false,     //Feito
    suddenDeath: true,  //-----
}



// ######### Examples ############## -- Uncomment

//suddenDeathExample();
//standExample();
//sameExample();
//plusExample();
//elemExample()



//this.lastPlay.firstCard = Carta que foi jogada (this.lastPlay.firstCard) 
//this.lastPlay.captured = lista com as cartas que foram viradas por ter um valor maior
//this.lastPlay.same = lista com as cartas que foram viradas pela regra "same"
//this.lastPlay.plus = lista com as cartas que foram viradas pela regra "plus"
//this.lastPlay.combo = lista com as cartas que foram viradas pela regra "combo"

module.exports = Game;