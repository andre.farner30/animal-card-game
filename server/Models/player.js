class Player {
    constructor(username) {
        this.username = username;
        this.hand = [];
        this.score = 5;
        this.decks = {
            deck1: {
                name: "---",
                cards: []
            },
            deck2: {
                name: "---",
                cards: []
            },
            deck3: {
                name: "---",
                cards: []
            },
            deck4: {
                name: "---",
                cards: []
            },
            deck5: {
                name: "---",
                cards: []
            }
        };
    }
}

Player.prototype.pickDeck = function (chosenDeck) {
    this.hand = this.decks[chosenDeck].cards;
    return this.hand;
};

Player.prototype.setDeck = function (deckSlot, deckName, cards) {
    this.decks[deckSlot].name = deckName;
    this.decks[deckSlot].cards = cards;
    return this.decks[deckSlot];
};

module.exports = Player;