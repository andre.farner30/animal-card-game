const socketio = require("socket.io");
const routes = require("./db-controller");

/*External libraries*/
//Easy Timer docs - https://albert-gonzalez.github.io/easytimer.js/
var { Timer } = require('easytimer.js');

var Card = require("../Models/card");
var Player = require("../Models/player");
var Game = require("../Models/game");

var rooms = [];
var timers = [];
var games = [];
var players = [];

module.exports.listen = function (app) {
    io = socketio.listen(app);
    io.set('origins', '*:*');
    io.of("/game").on('connection', function (socket) {
        console.log("server-side socket connected!");
        socket.on("disconnect", function () {
            console.log(socket.id + " diconnected");
        });

        socket.on('joinroom', function (room) {
            socket.memes = room.name;
            if (room.joinorhost == "host") {
                if (!rooms.includes(room.name)) {
                    socket.join(room.name);
                    rooms.push(room.name);
                    lobbyHost(room.name, room.player, (p) => {
                        io.of("/game").in(room.name).emit('lobbyleftset', { rules: options, player: p, room: room.name });
                    });
                } else {
                    io.of("/game").emit('fullLobby', true);
                }
            } else {
                if (rooms.includes(room.name)) {
                    io.of("/game").in(room.name).clients((error, clients) => {
                        if (error) throw error;
                        if (clients.length <= 1) {
                            socket.join(room.name);
                            lobbyVisitor(room.name, room.player, (p) => {
                                socket.emit('lobbyleftset', { rules: options, player: p, room: room.name });
                                socket.emit('opponentsetup', players.find(obj => obj.roomName == room.name).player1);
                                socket.to(room.name).emit('opponentsetup', p);
                            });
                        } else {
                            socket.emit('fullLobby', true);
                        }
                    });
                } else {
                    socket.emit('fullLobby', true);
                }
            }

            socket.on('gamelobbymsg', function (msg) {
                socket.emit('gamelobbymsg', { sent: true, msg: msg });
                socket.to(room.name).emit('gamelobbymsg', { sent: false, msg: msg });
            });

            socket.on('rulechange', function (msg) {
                options[msg.rule] = msg.checked;
                io.of("/game").in(room.name).emit('rulechange', msg);
            });

            socket.on('turntimechange', function (msg) {
                turnTime = msg;
                io.of("/game").in(room.name).emit('turntimechange', msg);
            });

            socket.on('playerisready', function (msg) {
                if (msg.state == 1) {
                    socket.to(room.name).emit('playerisready', msg.state);
                    var p1 = players.find(obj => obj.roomName == room.name).player1;
                    var p2 = players.find(obj => obj.roomName == room.name).player2;
                    if (msg.playerName == p1.username) {
                        p1.pickDeck(msg.deck);
                    } else if (msg.playerName == p2.username) {
                        p2.pickDeck(msg.deck);
                    }
                } else if (msg.state == 2) {
                    io.of("/game").in(room.name).emit('playerisready', msg.state);
                    var pls = [players.find(obj => obj.roomName == room.name).player1, players.find(obj => obj.roomName == room.name).player2];
                    setTimer(room.name, pls, socket);
                } else if (msg.state == 0) {
                    if (timers.includes(timers.find(obj => obj.roomName == room.name))) {
                        timers.find(obj => obj.roomName == room.name).timer.stop();
                        timers = timers.filter(t => t.roomName != room.name);
                    }
                    io.of("/game").in(room.name).emit('playerisready', msg.state);
                }
            });

            socket.on('playtrigger', function (msg) {
                var g = games.find(x => x.gameName == msg.game.gameName);
                var result = g.play(msg.cardIndex, msg.cardPos[0] - 1, msg.cardPos[1] - 1);
                if (result == 1 || result == 3) {
                    //setTurnTimer(room.name, msg.game.gameName, socket);
                    socket.emit("playtrigger", { gameState: g, madePlay: true });
                    socket.to(room.name).emit('playtrigger', { gameState: g, playedCardIndex: msg.cardIndex, madePlay: false });
                    g.printBoard();
                    if (result == 3) {
                        io.of('/game').to(room.name).emit('gameEnd', g);
                    }
                }
            });

            socket.on('gameConcede', function (msg) {
                var g = games.find(x => x.gameName == msg.gameInfo.gameName);
                io.of('/game').to(room.name).emit('gameEnd', g);
            });

            socket.on('postGame', function (msg) {
                
                // if (msg.cond == 1) {
                //     socket.to(room.name).emit('postGame', { cond: msg.cond, rematch: msg.rematch });
                // } else if (msg.cond == 2) {
                //     io.of("/game").in(room.name).emit('postGame', { cond: msg.cond, rematch: msg.rematch });
                //     var pls = [players.find(obj => obj.roomName == room.name).player1, players.find(obj => obj.roomName == room.name).player2];
                //     if (!msg.rematch) {
                //         io.of("/game").in(room.name).emit('gameRestart', { rules: options, players: pls, rematch: msg.rematch });
                //     }
                //     //setTimer(5, room.name, pls);
                // } else if (msg.state == 0) {
                //     if (timers.includes(timers.find(obj => obj.roomName == room.name))) {
                //         timers.find(obj => obj.roomName == room.name).timer.stop();
                //         timers = timers.filter(t => t.roomName != room.name);
                //     }
                //     io.of("/game").in(room.name).emit('playerisready', msg.state);
                // }
                if(msg.cond == 0){
                    io.of("/game").in(room.name).emit('postGame', { cond: 0, rematch: false });
                }
            });
        });
    });
    return io;
}

var options = {
    open: false,          //-----
    same: false,          //Feito
    plus: false,          //Feito
    combo: false,         //Feito
    elemental: false     //Feito
}

var turnTime = 15;

function setTimer(room, players, socket) {
    var timer = new Timer();
    timer.start({ countdown: true, startValues: { seconds: 5 } });
    timer.addEventListener('targetAchieved', function (e) {
        console.log("server-timer");
        console.log("A game has started!");
        const game = gameSetup(room, players[0], players[1], options);
        io.of("/game").in(room).emit('gamestart', { game: game, turnTime: turnTime });
        //setTurnTimer(room, game.gameName, socket);

        //timer = timers.find(t => t.roomName == room);
        // t.start({ countdown: true, startValues: { seconds: 30 } });
        // t.addEventListener('targetAchieved', function (e) {
        //     var g = games.find(x => x.gameName == msg.game.gameName);
        //     g.play()
        // });
        //players = players.filter(t => t.roomName != room.name);
    });
    for (let i = 0; i < timers.length; i++) {
        if (timers[i].roomName == room) {
            timers[i].timer = timer;
            return;
        }
    }
    timers.push({ roomName: room, timer: timer });
}

function startGameEvent(room, players) {

}

function setTurnTimer(room, gameName, socket) {
    let timer = new Timer();
    timer.start({ countdown: true, startValues: { seconds: turnTime } });
    timer.addEventListener('targetAchieved', function (e) {
        const g = games.find(x => x.gameName == gameName);
        g.isPlayer1 ? console.log(g.player1.username) : console.log(g.player2.username);
        g.isPlayer1 && socket.emit('turntimer') || socket.to(room).emit('turntimer');
        console.log("Turn timer event finished");
    });

    //Update timer array with new timer
    for (let i = 0; i < timers.length; i++) {
        if (timers[i].roomName == room) {
            timers[i].timer = timer;
        }
    }
}

function nextTurn() {

}

function turnTimerEvent(room) {

}

function getAllCards(callback) {
    let data = [];
    let citiesRef = routes.db.firestore().collection('cards');
    citiesRef.get()
        .then(snapshot => {
            snapshot.forEach(doc => {
                data.push(doc.data())
            });
            callback(data)
        });
}

function lobbyHost(room, player, socketCallback) {
    const hostPlayer = new Player(player.userName + " - H");
    getAllCards(function (cards) {
        Object.keys(player.decks).forEach(e => {
            if (player.decks[e].cards.length != 0) {
                let hand = [];
                player.decks[e].cards.forEach(card => {
                    let c = cards.find(x => x.id == card);
                    hand.push(new Card(c.id, c.name, c.left, c.right, c.up, c.down, c.elemental, c.rank, c.img));
                })
                hostPlayer.setDeck(e, player.decks[e].name, hand);
            }
        })
        players.push({ roomName: room, player1: hostPlayer, player2: undefined });
        socketCallback(hostPlayer);
    })
}

function lobbyVisitor(room, player, socketCallback) {
    const joinedPlayer = new Player(player.userName + " - V");
    getAllCards(function (cards) {
        Object.keys(player.decks).forEach(e => {
            if (player.decks[e].cards.length != 0) {
                let hand = [];
                player.decks[e].cards.forEach(card => {
                    let c = cards.find(x => x.id == card);
                    hand.push(new Card(c.id, c.name, c.left, c.right, c.up, c.down, c.elemental, c.rank, c.img));
                })
                joinedPlayer.setDeck(e, player.decks[e].name, hand);
            }
        })
        players.find(obj => obj.roomName == room).player2 = joinedPlayer;
        socketCallback(joinedPlayer);
    })
}

function gameSetup(gameName, player1, player2, rules) {
    var game;
    var whoPlays = Math.floor(Math.random() * 2);
    whoPlays == 1 ? game = new Game(gameName, player1, player2, rules) : game = new Game(gameName, player2, player1, rules);

    games.push(game);
    return game;
}