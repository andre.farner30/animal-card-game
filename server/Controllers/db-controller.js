const express = require('express')
const admin = require("firebase-admin");
const serviceAccount = require("./serviceAccountKey.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://animal-card-game-239621.firebaseio.com"
});

var router = express.Router();

//Buscar o documento do username
router.get("/user/:email", function (req, res) {
    let cityRef = admin.firestore().collection('users').doc(req.params.email);
    cityRef.get()
        .then(doc => {
            if (!doc.exists) {
                console.log('No such document!');
            } else {
                res.send(doc.data());
            }
        })
        .catch(err => {
            console.log('Error getting document', err);
            res.send(err);
        });
});

// buscar stats do user por email
router.get("/userstats/:email", function (req, res) {
    let cityRef = admin.firestore().collection('stats').doc(req.params.email);
    cityRef.get()
        .then(doc => {
            if (!doc.exists) {
                console.log('No such document!');
            } else {
                res.send(doc.data());
            }
        })
        .catch(err => {
            console.log('Error getting document', err);
            res.send(err);
        });
});

//Busca todos os documentos de stats
router.get("/stats", function (req, res) {
    let data = [];
    let citiesRef = admin.firestore().collection('stats');
    citiesRef.get()
        .then(snapshot => {
            snapshot.forEach(doc => {
                data.push(doc.data())
            });
            res.send(data);
        })
        .catch(err => {
            console.log('Error getting documents', err);
            res.send(err)
        });
});

//Buscar todos cartas por id
router.get("/users", function (req, res) {
    let data = [];
    let citiesRef = admin.firestore().collection('users');
    citiesRef.get()
        .then(snapshot => {
            snapshot.forEach(doc => {
                data.push(doc.data())
            });
            res.send(data);
        })
        .catch(err => {
            console.log('Error getting documents', err);
            res.send(err)
        });
});
// buscar carda por id
router.get("/cardid/:id", function (req, res) {
    let cityRef = admin.firestore().collection('cards').doc(req.params.id);
    cityRef.get()
        .then(doc => {
            if (!doc.exists) {
                console.log('No such document!');
            } else {
                res.send(doc.data());
            }
        })
        .catch(err => {
            console.log('Error getting document', err);
            res.send(err);
        });
});

//Buscar todos cartas por id
router.get("/cards", function (req, res) {
    let data = [];
    let citiesRef = admin.firestore().collection('cards');
    citiesRef.get()
        .then(snapshot => {
            snapshot.forEach(doc => {
                data.push(doc.data())
            });
            res.send(data);
        })
        .catch(err => {
            console.log('Error getting documents', err);
            res.send(err)
        });
});

//Buscar todos cartas por id
router.get("/usercards/:email", function (req, res) {
    let data = [];
    let cards = [];
    let cityRef = admin.firestore().collection('user-cards').doc(req.params.email);
    cityRef.get()
        .then(doc => {
            doc.data().cards.forEach(card => {
                cards.push(card)
            })
        }).then(async () => {
            await Promise.all(
                cards.map(
                    async card => {
                        let cityRef = admin.firestore().collection('cards').doc("" + card);
                        await cityRef.get().then(doc => {
                            data.push(doc.data())
                        })
                    }
                )
            )
            res.send(data)
        })
        .catch(err => {
            console.log('Error getting document', err);
            res.send(err);
        });
});

router.post("/newdeck/:email", function (req, res) {
    var deckId = "deck" + req.body.deckId;
    var deck = req.body.deck;

    var update = {};
    update[`decks.${deckId}`] = deck;
    var ref = admin.firestore().collection('users').doc("" + req.params.email);
    ref.update(update).then(() => {
        res.send(req.params.email);
    });
});

router.post("/deldeck/:email", function (req, res) {
    var deckId = "deck" + req.body.deckId;
    var deck = { cards: [], name: "---" };

    var update = {};
    update[`decks.${deckId}`] = deck;
    var ref = admin.firestore().collection('users').doc("" + req.params.email);
    ref.update(update).then(() => {
        res.send(req.params.email);
    });
});

router.post("/newcards/:email", function (req, res) {
    const packs = req.body.cards;
    let arr;
    var ref = admin.firestore().collection('user-cards').doc("" + req.params.email);
    ref.get()
        .then(doc => {
            arr = doc.data().cards;
        })
        .then(() => {
            packs.forEach(a => {
                if (!arr.includes(a)) {
                    arr.push(a)
                }
            })
            arr.sort(function (a, b) { return a - b });
            var update = {};
            update["cards"] = arr;
            ref.update(update).then(() => {
                res.send(req.params.email);
            })
        })
        .catch(err => {
            console.log('Error getting documents', err);
        });
});

exports.router = router;
exports.db = admin;
