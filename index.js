const path = require("path");
const express = require("express");
const app = express();
const http = require('http').Server(app);
const bodyParser = require('body-parser');
const io = require("./server/Controllers/game-controller").listen(http);
const port = process.env.NODE_PORT || 5000;
const routes = require("./server/Controllers/db-controller");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/requests', routes.router);

// Set your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY || 'sk_test_p9vXAGHOfgOPg0jI1Iro8bOM007OwzdAPj');

//Unique customer id
const uuid = require("uuid/v4");

app.use(express.static(path.join(__dirname, 'swap-shelter/build')));
app.use(express.static(path.join(__dirname, 'server/public')));

app.post("/checkout", async (req, res) => {

	let error;
	let status;
	try {
		const { product, token, userEmail } = req.body;

		const customer = await stripe.customers.create({
			email: token.email,
			source: token.id
		});

		const idempotency_key = uuid();
		const charge = await stripe.charges.create(
			{
				amount: product.price * 100,
				currency: product.currency,
				customer: customer.id,
				receipt_email: token.email,
				description: `Purchased ${product.name}`
			},
			{
				idempotency_key
			}
		);
		console.log("Charge:", { charge });
		status = "success";

		//Mandar os treats para a conta atualmente logged in para o firebase
		const docRef = routes.db.firestore().collection('users').doc(userEmail);
		const incfunc = function (inc_value) { return routes.db.firestore.FieldValue.increment(inc_value) };
		let increment = incfunc(charge.amount);
		docRef.update({
			treats : increment
		});
	} catch (error) {
		console.error("Error", error);
		status = "failure";
	}

	res.json({ error, status });
});

// io.on('connection', function (socket) {
// 	console.log("server-side socket connected!");
// });

app.get("*/", function (req, res) {
	res.sendFile(path.join(__dirname, 'swap-shelter/build/index.html'));
});

const server = http.listen(port, function () {
	const host = server.address().address === "::" ? "localhost" :
		server.address().address;

	const port = server.address().port;
	console.log("Example app listening at http://%s:%s/", host, port);
});